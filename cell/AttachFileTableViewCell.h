//
//  AttachFileTableViewCell.h
//  SeoNote
//
//  Created by 신서희 on 30/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttachFileTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *ivAttachFile;
@property (strong, nonatomic) UILabel *lbfileName;

@end

NS_ASSUME_NONNULL_END
