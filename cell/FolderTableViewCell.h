//
//  FolderTableViewCell.h
//  SeoNote
//
//  Created by hjunnn kim on 01/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FolderTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *ivIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UILabel *lbCount;
@property (weak, nonatomic) IBOutlet UIImageView *ivPw;


@end

NS_ASSUME_NONNULL_END
