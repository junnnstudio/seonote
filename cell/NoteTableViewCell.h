//
//  NoteTableViewCell.h
//  SeoNote
//
//  Created by 신서희 on 04/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoteTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ivIcon;
@property (weak, nonatomic) IBOutlet UILabel *lbNote;

@end

NS_ASSUME_NONNULL_END
