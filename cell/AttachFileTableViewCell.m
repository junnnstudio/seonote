//
//  AttachFileTableViewCell.m
//  SeoNote
//
//  Created by 신서희 on 30/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "AttachFileTableViewCell.h"

@implementation AttachFileTableViewCell


-(void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentRect = self.contentView.bounds;
    CGFloat boundX = contentRect.origin.x;
    CGRect frame;
    
    //frame = CGRectMake(self.contentView.frame.size.width - 64, 6, 54, 54);
    frame = CGRectMake(boundX + 10, 6, 54, 54);
    _ivAttachFile.frame = frame;
    
    frame = CGRectMake(_ivAttachFile.frame.size.width+20, 20, self.contentView.frame.size.width * 0.6, 25);
    _lbfileName.frame = frame;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        _lbfileName = [[UILabel alloc] init];
        _lbfileName.text = @"";
        _lbfileName.font = [UIFont fontWithName:@"AppleSDGothicNeo-Regular" size:16.0F];
        _lbfileName.textColor = RGB(34, 34, 34);
        _lbfileName.textAlignment = NSTextAlignmentLeft;
        _lbfileName.backgroundColor = [UIColor clearColor];
        
        _ivAttachFile = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@""]];
        
        [self.contentView addSubview:_lbfileName];
        [self.contentView addSubview:_ivAttachFile];
        
        self.contentView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


@end
