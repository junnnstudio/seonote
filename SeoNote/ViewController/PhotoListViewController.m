//
//  PhotoListViewController.m
//  SeoNote
//
//  Created by 신서희 on 21/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "PhotoListViewController.h"

#import "AttachMD.h"
#import "AttachDBMD.h"
#import "ConstantsDB.h"



#define TEMP_TAG 1000

@interface PhotoListViewController ()

@end

@implementation PhotoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _db = [[ConstantsDB alloc] initDatabase];
    
    // Do any additional setup after loading the view.
    
    _photoScrollView.contentSize = CGSizeMake(self.view.frame.size.width * [_arrPhoto count], _photoScrollView.frame.size.height);
    
    for(int idx = 0; idx < _arrPhoto.count; idx++){
        
        UIScrollView *tempScroll = [[UIScrollView alloc] init];
        tempScroll.minimumZoomScale = 1.0F;
        tempScroll.maximumZoomScale = 2.0F;

        tempScroll.delegate = self;
        
        UIImageView *tempImageView = [[UIImageView alloc]init];
        tempImageView.tag = TEMP_TAG + idx;
        NSString *fileName = [[_arrPhoto[idx] m_file_name] lastPathComponent];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", _db.documentsDir, fileName];
        
        UIImage *imageView = [UIImage imageNamed:filePath];
        tempImageView.image = imageView;
        tempImageView.contentMode = UIViewContentModeScaleAspectFit;
        
        CGFloat xPosition = self.view.frame.size.width * idx;
        
        tempScroll.frame = CGRectMake(xPosition, 0, self.view.frame.size.width, _photoScrollView.frame.size.height);
        tempImageView.frame = CGRectMake(0, 0, self.view.frame.size.width, _photoScrollView.frame.size.height);
        
        [tempScroll addSubview:tempImageView];
        [_photoScrollView addSubview:tempScroll]; //scrollView에 만든 imageView 붙이기
        
    }
    
    _photoScrollView.contentOffset = CGPointMake(self.view.frame.size.width * _getPhotoNo+1, 0);
    int page = _photoScrollView.contentOffset.x / _photoScrollView.frame.size.width;
    _navigationItem.title = [NSString stringWithFormat:@"Photo (%d / %lu)", page+1, _arrPhoto.count];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    UIImageView *temp = [self.view viewWithTag:(TEMP_TAG + _getPhotoNo)];
    
    return temp;
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    _getPhotoNo = _photoScrollView.contentOffset.x / _photoScrollView.frame.size.width;
    
    _navigationItem.title = [NSString stringWithFormat:@"Photo (%ld / %lu)", _getPhotoNo + 1, (unsigned long)_arrPhoto.count];
}


- (IBAction)touchesCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
