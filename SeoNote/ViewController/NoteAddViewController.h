//
//  NoteAddViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#import <CoreLocation/CoreLocation.h>

#import "NoteMD.h"

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN

@interface NoteAddViewController : UIViewController<UITextViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIStoryboard *sb;

@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) ConstantsDB *db;
@property (nonatomic) UIImagePickerController *tempImage;

@property (weak, nonatomic) IBOutlet UITableView *attachTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstrainTableView;

@property (weak, nonatomic) IBOutlet UITextView *addTextView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (weak, nonatomic) IBOutlet UINavigationBar *naviBar;

@property (strong, nonatomic) NSString *strImageName;
@property (nonatomic) UIImageView *tempImageView;
@property (nonatomic) NSMutableArray *tempImageArray;

@property (nonatomic) UILabel *lbl;

@property (nonatomic) NSInteger getFolderNo;


@property (strong, nonatomic) CLLocationManager *locationManager;
@property (assign, nonatomic) float latitude;
@property (assign, nonatomic) float longitude; 

- (IBAction)touchesPhotoBtn:(id)sender;
- (IBAction)touchesCameraBtn:(id)sender;
- (IBAction)touchesSaveBtn:(id)sender;
- (IBAction)touchesBtnShowTable:(id)sender;

@end

NS_ASSUME_NONNULL_END
