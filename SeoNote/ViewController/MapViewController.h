//
//  MapViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 01/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAnnotation.h"
#import "FBClusteringManager.h"


#import "ConstantsDB.h"

@class MapContentMD;

@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface MapViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) NSMutableArray<CustomAnnotation *> *arrFBClusteringManager;
@property (strong, nonatomic) FBClusteringManager *clusteringManager;
@property (strong, nonatomic) CustomAnnotation *customAnnotation;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) MKPolyline *polyLine;

@property (strong, nonatomic) ConstantsDB *db;
@property (strong, nonatomic) NSMutableArray<MapContentMD*> *arrMapContent; 

@property (weak, nonatomic) IBOutlet MKMapView *map;

@property (strong, nonatomic) NSString *strFindUniqueSavePath;

- (IBAction)touchesBtnMap:(id)sender;

@end

NS_ASSUME_NONNULL_END
