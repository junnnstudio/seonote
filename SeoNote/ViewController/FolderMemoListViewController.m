//
//  FolderMemoListViewController.m
//  SeoNote
//
//  Created by 신서희 on 12/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "FolderMemoListViewController.h"
#import "NoteMD.h"
#import "NoteDBMD.h"

#import "ConstantsDB.h"

#import "NoteAddViewController.h"
#import "NoteDetailViewController.h"

#import "NoteTableViewCell.h"

@interface FolderMemoListViewController ()

@end

@implementation FolderMemoListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    _db = [[ConstantsDB alloc] initDatabase];
    
    _arrNote = [[NSMutableArray alloc] init];
    
    _folderDBMD = [_db selectFolderByfolderID:_getFolderNo];

    _naviBar.topItem.title = _folderDBMD.m_name;
    [_naviBar setBarTintColor:RGB(_folderDBMD.m_red, _folderDBMD.m_green, _folderDBMD.m_blue)];
    [_naviBar setTranslucent:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = RGB(_folderDBMD.m_red, _folderDBMD.m_green, _folderDBMD.m_blue);
    }
    
    _arrNote = [[_db selectNoteFolderId:_getFolderNo] mutableCopy];
    [_noteTableView reloadData];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
//    ios7 이후로 아래 메소드 쓰리말레
//    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
//    ios7 이후로 아래 메소드 쓰리말레
//    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    return NO;
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrNote.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *noteCell   = @"identifierNoteCell";
    
    NoteTableViewCell *cell = (NoteTableViewCell*)[tableView dequeueReusableCellWithIdentifier:noteCell];
    
    if(cell == nil) {
        cell = [[NoteTableViewCell alloc] initWithFrame:CGRectZero];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NoteDBMD *tempNoteDB = [_arrNote objectAtIndex:indexPath.row];
    
    //로우 인덱스만큼 folder 배열에서 값을 가져와 cell에 뿌린다.
    cell.lbNote.text = tempNoteDB.m_contents;
    cell.lbNote.textColor = [UIColor darkGrayColor];
    cell.lbNote.lineBreakMode = NSLineBreakByTruncatingTail;
    
    NSArray *arrAttach = [_db selectAttachNoteId:tempNoteDB.m_noteID];
    if([arrAttach count] > 0){
        cell.ivIcon.image = [UIImage imageNamed:@"attach"];
    }else{
        cell.ivIcon.image = [UIImage imageNamed:@"plus"];
    }
    
    return cell;
}

#pragma mark - uitableviewdelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0) {
        NoteDBMD *tempNoteDb = [_arrNote objectAtIndex:indexPath.row];
        NoteDetailViewController *noteDetailViewController = [_sb instantiateViewControllerWithIdentifier:@"detailStroyboard"];
        noteDetailViewController.getNo = tempNoteDb.m_noteID;
        noteDetailViewController.folderNumber = _getFolderNo; 
        
        [self presentViewController:noteDetailViewController animated:YES completion:nil];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //오->왼으로 슬라이드시
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 메모 삭제
        NoteDBMD *tempNoteDb = [_arrNote objectAtIndex:indexPath.row];
        [_db deleteNote:tempNoteDb.m_noteID];
        [_db deleteAttachByNoteID:tempNoteDb.m_noteID];
        
        [_arrNote removeObjectAtIndex:indexPath.row];
    
        [_noteTableView reloadData];
    }
}

- (IBAction)touchesCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)touchesNoteAddBtn:(id)sender {
    NoteAddViewController *noteAddViewController = [_sb instantiateViewControllerWithIdentifier:@"noteaddviewcontroller"];
    noteAddViewController.getFolderNo = _getFolderNo;
    
    [noteAddViewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:noteAddViewController animated:TRUE completion:nil];
}

@end
