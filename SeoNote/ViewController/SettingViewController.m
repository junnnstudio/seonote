//
//  SettingViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "SettingViewController.h"

#import "PasswordViewController.h"
#import "SettingHiddenViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _arrFolder = [[NSMutableArray alloc] init];
    _arrNote   = [[NSMutableArray alloc] init];
    
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    _db = [[ConstantsDB alloc] initDatabase];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    _arrFolder = [[_db selectFolderAll] mutableCopy];
    _arrNote = [[_db selectNoteAll] mutableCopy];
    [_settingTableView reloadData];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 45.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 2;
    }else {
        return 1;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *infoCell = @"identifierInfoCell";
    static NSString *hiddenCell = @"identifierHiddenCell";
    
    if(indexPath.section == 0) {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:infoCell];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = [NSString stringWithFormat:@"Note : %lu ", _arrNote.count]; //노트갯수
                break;
            case 1:
                cell.textLabel.text = [NSString stringWithFormat:@"Folder : %lu 개", _arrFolder.count]; //폴더갯수
                break;
            default:
                break;
        }
        return cell;
        
    }else {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:hiddenCell];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.textLabel.text = @"Enable password lock";
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    NSString *tempTitle = @"";
    
    if(section == 0){
        tempTitle = @" Notes Usage Status";
    }else{
        tempTitle = @" Create password";
    }
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = tempTitle;
    header.textLabel.textColor = [UIColor lightGrayColor];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0F;
}



#pragma mark - uitableviewdelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 1 && indexPath.row == 0) {
        _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController = nil;
        
         NSString *tempPw = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
        
        if(tempPw != nil) {
            viewController = [_sb instantiateViewControllerWithIdentifier:@"passwordviewcontroller"];
        }else {
            viewController = [_sb instantiateViewControllerWithIdentifier:@"settinghiddenviewcontroller"];
        }
        
        [viewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
        [self presentViewController:viewController animated:true completion:nil];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchesBtnClose:(id)sender {
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
