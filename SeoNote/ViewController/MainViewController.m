//
//  MainViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "MainViewController.h"
#import "NoteDetailViewController.h"
#import "FolderMemoListViewController.h"

#import "FolderTableViewCell.h"
#import "NoteTableViewCell.h"


#define kButtonSetting    100
#define kButtonSearch     200
#define kButtonMap        300

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //배열 초기화
    _arrFolder = [[NSMutableArray alloc] init];
    _arrNote   = [[NSMutableArray alloc] init];
    
    //메인에 오른쪽하단 버튼 활성화 유/무
    _isOpenMenu = false;
    
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //db 초기화
    _db = [[ConstantsDB alloc] initDatabase];
    
    _arrColorTheme = [[NSArray alloc] initWithObjects:@"",@"_blue", @"_dark_blue", @"_gray", @"_green_one", @"_green_two", @"_orange_one", @"_orange_two", @"_purple", @"_red_one", @"_red_two", @"_yellow", nil];
    
    [self createMenuButton];
}

- (void)viewWillAppear:(BOOL)animated{
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = RGB(255, 255, 255);
    }
    
    _strTheme = [_arrColorTheme objectAtIndex:arc4random_uniform((uint32_t)[_arrColorTheme count])];
    
    [_btnMap setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"map%@", _strTheme]] forState:UIControlStateNormal];
    [_btnSetting setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"setting%@", _strTheme]] forState:UIControlStateNormal];
    [_btnSearch setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"search%@", _strTheme]] forState:UIControlStateNormal];
    [_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"open%@", _strTheme]] forState:UIControlStateNormal];
    
    //암호화 설정 유무 NSUserDefaults 를 이용함 (내부 DB같은 것)
    _tempPw = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    _arrFolder = [[_db selectFolderAll] mutableCopy]; //페이지가 나타날때마다 폴더리스트를 가져온다.
    _arrNote = [[_db selectNoteFolderId:0] mutableCopy]; //페이지가 나타날때마다 노트리스트를 가져온다.
    [_mainTableView reloadData]; //메인 테이블뷰의 데이터를 리로드한다. (새로고침)
}

- (void) viewDidDisappear:(BOOL)animated{
    if(_isOpenMenu) { //버튼이 활성화 되어있을 때 -> 다시 숨겨준다.
        CGRect frame = _btnSetting.frame;
        frame.origin.y = _btnMenu.frame.origin.y;
        
        [UIView animateWithDuration:0.3 delay:0.1 options:0 animations:^{
            self->_btnSetting.frame = frame;
            self->_btnSearch.frame  = frame;
            self->_btnMap.frame = frame;
        } completion:^(BOOL finished){
            [self->_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"open%@", self->_strTheme]] forState:UIControlStateNormal];
            self->_isOpenMenu = false;
            
            self->_btnMap.alpha = 0.0F;
            self->_btnSearch.alpha  = 0.0F;
            self->_btnSetting.alpha = 0.0F;
        }];
    }
    
}


- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}



//오른쪽 하단 설정/검색 버튼 생성
- (void)createMenuButton {
    //위치
    float positionX = self.view.frame.size.width - 60;
    float positionY = self.view.frame.size.height - 60;
    
    float btnW = 35;
    float btnH = 35;
    
    //설정화면 버튼 생성
    _btnSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnSetting.frame = CGRectMake(positionX, positionY, btnW, btnH);
    _btnSetting.tag = kButtonSetting;
//    [_btnSetting setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"setting%@", _strTheme]] forState:UIControlStateNormal];
    [_btnSetting addTarget:self action:@selector(touchesBtnMove:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnSetting];
    _btnSetting.alpha = 0.0F;
    
//   맵 버튼
    _btnMap = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnMap.frame = CGRectMake(positionX, positionY, 40, 40);
    _btnMap.tag = kButtonMap;
//    [_btnMap setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"map%@", _strTheme]] forState:UIControlStateNormal];
    [_btnMap addTarget:self action:@selector(touchesBtnMove:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnMap];
    _btnMap.alpha = 0.0F;
    
    //검색화면 버튼 생성
    _btnSearch = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnSearch.frame = CGRectMake(positionX, positionY, btnW, btnH);
    _btnSearch.tag = kButtonSearch;
//    [_btnSearch setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"search%@", _strTheme]] forState:UIControlStateNormal];
    [_btnSearch addTarget:self action:@selector(touchesBtnMove:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnSearch];
    _btnSearch.alpha = 0.0F;
    
    _btnMenu = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnMenu.frame = CGRectMake(positionX, positionY, btnW, btnH);
//    [_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"open%@", _strTheme]] forState:UIControlStateNormal];
    [_btnMenu addTarget:self action:@selector(touchesBtnMenu:) forControlEvents:UIControlEventTouchUpInside];
    _btnMenu.alpha = 0.7F;
    [self.view addSubview:_btnMenu];

}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0F; //테이블 row 높이
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //섹션별 row 수
    if(section == 0) {
        return [_arrFolder count];
    }else {
        return [_arrNote count];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //섹션수 (구역)
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *folderCell = @"identifierFolderCell"; //cell 을 storyboard 에서 생성시 고유한 id값을 기입.
    static NSString *noteCell   = @"identifierNoteCell";
    
    if(indexPath.section == 0) {
        FolderTableViewCell *cell = (FolderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:folderCell];
        
        if(cell == nil) { //셀이 비어있다면
            cell = [[FolderTableViewCell alloc] initWithFrame:CGRectZero]; // 셀 초기화
        }
        
        FolderDBMD *tempFolderDB = [_arrFolder objectAtIndex:indexPath.row];
        //로우 인덱스만큼 folder 배열에서 값을 가져와 cell에 뿌린다.
        cell.lbTitle.text = tempFolderDB.m_name;
        cell.lbTitle.textColor = RGB(tempFolderDB.m_red, tempFolderDB.m_green, tempFolderDB.m_blue);
        cell.lbTitle.lineBreakMode = NSLineBreakByTruncatingTail;
        cell.lbCount.backgroundColor = RGB(tempFolderDB.m_red, tempFolderDB.m_green, tempFolderDB.m_blue);
        cell.lbCount.text = [NSString stringWithFormat:@"%lu", (unsigned long)[[_db selectNoteFolderId:tempFolderDB.idx] count]];
        
        cell.ivIcon.image = [[UIImage imageNamed:@"folder"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        cell.ivIcon.tintColor = RGB(tempFolderDB.m_red, tempFolderDB.m_green, tempFolderDB.m_blue);
        
        cell.ivPw.image = [UIImage imageNamed:@""];
        
        if(_tempPw != nil){ //암호화가 지정되있는 상태면
            if([tempFolderDB.m_hidden_yn  isEqual: @"Y"]){ //폴더의 암호화설정 유무를 물어보고
                cell.ivPw.image = [UIImage imageNamed:@"secret"];
//                cell.accessoryType = UITableViewCellAccessoryCheckmark; //셀의 스타일을 변경함. (체크마크 스타일)
            }
        }
        
//        UILabel *lbTemp = [[UILabel alloc] initWithFrame:CGRectMake(cell.contentView.frame.size.width - 30, 0, 20, 20)];
//        lbTemp.text = [NSString stringWithFormat:@"%lu", (unsigned long)[[_db selectNoteFolderId:tempFolderDB.idx] count]];
//        lbTemp.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:10.0F];
//        lbTemp.textColor = [UIColor whiteColor];
//        lbTemp.textAlignment = NSTextAlignmentCenter;
//
//        lbTemp.numberOfLines   = 1;
//        lbTemp.adjustsFontSizeToFitWidth = YES;
//
//        lbTemp.layer.cornerRadius = 8;
//        lbTemp.layer.masksToBounds = true;
//
//        cell.lbTitle.backgroundColor = RGB(tempFolderDB.m_red, tempFolderDB.m_green, tempFolderDB.m_blue);
//
//        [cell.contentView addSubview:lbTemp];
        
        return cell;
        
    }else { //노트리스트 섹션
//        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:noteCell];
//        if(cell == nil) {
//            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noteCell];
//        }
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//
//        NoteDBMD *tempNoteDB = [_arrNote objectAtIndex:indexPath.row];
//        cell.textLabel.text = tempNoteDB.m_contents;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        cell.textLabel.textColor = [UIColor darkGrayColor];
//
//        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
//
//        NSArray *arrAttach = [_db selectAttachNoteId:tempNoteDB.m_noteID];
//        if([arrAttach count] > 0){
//            cell.imageView.image = [UIImage imageNamed:@"secret"];
//        }else{
//            cell.imageView.image = [UIImage imageNamed:@""];
//        }
        
        NoteTableViewCell *cell = (NoteTableViewCell*)[tableView dequeueReusableCellWithIdentifier:noteCell];
        
        if(cell == nil) { //셀이 비어있다면
            cell = [[NoteTableViewCell alloc] initWithFrame:CGRectZero]; // 셀 초기화
        }
        
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NoteDBMD *tempNoteDB = [_arrNote objectAtIndex:indexPath.row];
        
        //로우 인덱스만큼 folder 배열에서 값을 가져와 cell에 뿌린다.
        cell.lbNote.text = tempNoteDB.m_contents;
        cell.lbNote.textColor = [UIColor darkGrayColor];
        cell.lbNote.lineBreakMode = NSLineBreakByTruncatingTail;
        
        NSArray *arrAttach = [_db selectAttachNoteId:tempNoteDB.m_noteID];
        if([arrAttach count] > 0){
            cell.ivIcon.image = [UIImage imageNamed:@"attach"];
        }else{
            cell.ivIcon.image = [UIImage imageNamed:@"plus"];
        }
        
        return cell;
    }
}

//폴더와 리스트 header 텍스트와 텍스트 컬러 설정
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    NSString *tempTitle = @"";
    
    if(section == 0){
        tempTitle = @" folder";
    }else{
        tempTitle = @" memo";
    }
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = tempTitle;
    header.textLabel.textColor = [UIColor lightGrayColor];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0F; //헤더 height 
}

- (void)selectRowAtIndexPath:(NSIndexPath *)indexPath
                    animated:(BOOL)animated
              scrollPosition:(UITableViewScrollPosition)scrollPosition{
    
}
#pragma mark - uitableviewdelegate
//row 선택시 이벤트
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //선택시 이동
    if(indexPath.section == 0) { //눌린 로우가 폴더 섹션이라면 폴더별 노트리스트 화면으로 이동.
        FolderDBMD *tempFolderDb = [_arrFolder objectAtIndex:indexPath.row];
        _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FolderMemoListViewController *folderMemoListViewController = [_sb instantiateViewControllerWithIdentifier:@"foldermemolistviewcontroller"];
        
        if( (_tempPw != nil) && ([tempFolderDb.m_hidden_yn isEqual: @"Y"]) ){ //폴더가 비밀폴더 설정되어있을때
            //alert 생성
            UIAlertController *pwAlert = [UIAlertController alertControllerWithTitle:@"Confirm Password" message:@"Please enter a password" preferredStyle:UIAlertControllerStyleAlert];
            [pwAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
                textField.placeholder = @"Please enter a password";
                textField.keyboardType = UIKeyboardTypeNumberPad;
                textField.secureTextEntry = YES;
            }];
            
            UIAlertController *doneAlert = [UIAlertController alertControllerWithTitle:nil message:@"The password is different." preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *doneOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [doneAlert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [doneAlert addAction:doneOk];
            
            UIAlertAction *done = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                if([self->_tempPw isEqualToString:pwAlert.textFields.lastObject.text]){
                    //설정되어있는 패스워드와 alert 에 입력한 비밀번호가 같으면
                    folderMemoListViewController.getFolderNo = tempFolderDb.idx; //해당 폴더 id를 넘긴다.
                    [folderMemoListViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
                    //화면이동.
                    [self presentViewController:folderMemoListViewController animated:true completion:nil];
                }else{
                    [self presentViewController:doneAlert animated:YES completion:nil];
                    //아니면 비밀번호 다르다는 alert 띄운다.
                }
            }];
            
            //cancel 버튼액션
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [pwAlert dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [pwAlert addAction:done];
            [pwAlert addAction:cancel];
            
            [self presentViewController:pwAlert animated:YES completion:nil];
        }else{//비밀폴더 설정 안되어있으면 바로 화면이동.
            folderMemoListViewController.getFolderNo = tempFolderDb.idx;
            
            [folderMemoListViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
            [self presentViewController:folderMemoListViewController animated:true completion:nil];
        }
        
        
    }else{
        //폴더 섹션이 아닌 노트 섹션일때는 바로 선택한 노트의 상세화면으로 이동
        NoteDBMD *tempNoteDb = [_arrNote objectAtIndex:indexPath.row];
        
        NoteDetailViewController *noteDetailViewController = [_sb instantiateViewControllerWithIdentifier:@"detailStroyboard"];
        noteDetailViewController.getNo = tempNoteDb.m_noteID; //노트 아이디 전달
        [self presentViewController:noteDetailViewController animated:YES completion:nil];
    }
    
    // 선택한 행이 계속 하이라이트 상태로 표시할 필요가 없기 때문에 선택을 해제한다.
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL returnValue = true;
    return returnValue;
}

- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    if(_isOpenMenu) { //버튼이 활성화 되어있을 때 -> 다시 숨겨준다.
        CGRect frame = _btnSetting.frame;
        frame.origin.y = _btnMenu.frame.origin.y;
        
        [UIView animateWithDuration:0.3 delay:0.1 options:0 animations:^{
            self->_btnSetting.frame = frame;
            self->_btnSearch.frame  = frame;
            self->_btnMap.frame = frame;
        } completion:^(BOOL finished){
            [self->_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"open%@", self->_strTheme]] forState:UIControlStateNormal];
            self->_isOpenMenu = false;
            
            self->_btnMap.alpha = 0.0F;
            self->_btnSearch.alpha  = 0.0F;
            self->_btnSetting.alpha = 0.0F;
        }];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //오->왼으로 슬라이드시
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 삭제
        if(indexPath.section == 0) {
            NSString *strMessage = @"When you delete a folder, \n all the sub-notes are deleted. \nDo you want to delete it?";
            
            UIAlertController *a = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
            NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:strMessage];
            [att addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:18.0] range:NSMakeRange(20, 9)];
            [att addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(20, 9)];
            [a setValue:att forKey:@"attributedTitle"];
            
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                FolderDBMD *tempFolderDB = [self->_arrFolder objectAtIndex:indexPath.row];
                
                NSArray *tempNoteArr = [self->_db selectNoteFolderId:tempFolderDB.idx];
                
                for(NoteDBMD *tempNote in tempNoteArr) {
                    [self->_db deleteAttachByNoteID:tempNote.m_noteID];
                }
                
                [self->_db deleteNoteByFolderID:tempFolderDB.idx];
                [self->_db deleteFolder:tempFolderDB.idx];
                
                [self->_arrFolder removeObjectAtIndex:indexPath.row];
                [self->_mainTableView reloadData];
                
                [a dismissViewControllerAnimated:YES completion:nil];
            }];
            UIAlertAction *calcelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [a dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [a addAction:calcelAction];
            [a addAction:deleteAction];
            
            
            [self presentViewController:a animated:YES completion:nil];
        }else {
            // 메모 삭제
            UIAlertController *a = [UIAlertController alertControllerWithTitle:@"Delete note" message:@"Delete the note." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *deleteAction = [UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
                
                NoteDBMD *tempNoteDb = [self->_arrNote objectAtIndex:indexPath.row];
                [self->_db deleteNote:tempNoteDb.m_noteID];
                [self->_db deleteAttachByNoteID:tempNoteDb.m_noteID];
                
                [self->_arrNote removeObjectAtIndex:indexPath.row];
                [self->_mainTableView reloadData];
                
                [a dismissViewControllerAnimated:YES completion:nil];
            }];
            UIAlertAction *calcelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [a dismissViewControllerAnimated:YES completion:nil];
            }];
            
            [a addAction:calcelAction];
            [a addAction:deleteAction];
            
            [self presentViewController:a animated:YES completion:nil];
        }
    }
}

- (void)touchesBtnMove:(id)sender {
    UIButton *tempBtn = (UIButton*)sender;
    
    UIViewController *viewController;
    if(tempBtn.tag == kButtonSetting) { //오른쪽 하단 버튼 설정버튼일때
        viewController = [_sb instantiateViewControllerWithIdentifier:@"settingviewcontroller"];
    }else if(tempBtn.tag == kButtonSearch){ //오른쪽 하단 버튼 검색버튼일때
        viewController = [_sb instantiateViewControllerWithIdentifier:@"searchviewcontroller"];
    }else if(tempBtn.tag == kButtonMap) {
        viewController = [_sb instantiateViewControllerWithIdentifier:@"mapviewcontroller"];
    }
    
    //화면이동.
    [viewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [self presentViewController:viewController animated:TRUE completion:nil];
}

- (void)touchesBtnMenu:(id)sender {
    if(_isOpenMenu) { //버튼이 활성화 되어있을 때 -> 다시 숨겨준다.
        CGRect frame = _btnSetting.frame;
        frame.origin.y = _btnMenu.frame.origin.y;
        
        [UIView animateWithDuration:0.3 delay:0.1 options:0 animations:^{
            self->_btnSetting.frame = frame;
            self->_btnSearch.frame  = frame;
            self->_btnMap.frame = frame;
        } completion:^(BOOL finished){
            [self->_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"open%@", self->_strTheme]] forState:UIControlStateNormal];
            self->_isOpenMenu = false;
            
            self->_btnMap.alpha = 0.0F;
            self->_btnSearch.alpha  = 0.0F;
            self->_btnSetting.alpha = 0.0F;
        }];
    }else { // 버튼이 비활성화일때 -> 활성화해준다.
        _btnMap.alpha = 0.7F;
        _btnSearch.alpha  = 0.7F;
        _btnSetting.alpha = 0.7F;
        
        float gap = 30.0F;
        
        CGRect frameSearch  = _btnSearch.frame;
        CGRect frameSetting = _btnSetting.frame;
        CGRect frameMap    = _btnMap.frame;
        
        frameSetting.origin.y  = _btnMenu.frame.origin.y - _btnMenu.frame.size.height - gap;
        frameSearch.origin.y = _btnMenu.frame.origin.y - (_btnMenu.frame.size.height + gap)*2;
        frameMap.origin.y    = _btnMenu.frame.origin.y - (_btnMenu.frame.size.height + gap)*3;

        [UIView animateWithDuration:0.3 delay:0.1 options:0 animations:^{
            self->_btnSetting.frame = frameSearch;
            self->_btnSearch.frame  = frameSetting;
            self->_btnMap.frame = frameMap;
        } completion:^(BOOL finished){
            [self->_btnMenu setBackgroundImage:[UIImage imageNamed:[NSString stringWithFormat:@"close%@", self->_strTheme]] forState:UIControlStateNormal];
            self->_isOpenMenu = true;
        }];
    }
}
- (IBAction)touchesBtnNoteAdd:(id)sender {
    //노트 추가버튼
    UIViewController *viewController = [_sb instantiateViewControllerWithIdentifier:@"noteaddviewcontroller"];
    
    [viewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:viewController animated:TRUE completion:nil];
    
}

- (IBAction)touchesBtnFolderAdd:(id)sender {
    //폴더 추가버튼
    UIViewController *viewController = [_sb instantiateViewControllerWithIdentifier:@"folderaddviewcontroller"];
    
    [viewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:viewController animated:TRUE completion:nil];
}



- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _btnMenu.alpha    = 0.3f;
    
    if(_isOpenMenu) {
        _btnSearch.alpha  = 0.3f;
        _btnSetting.alpha = 0.3f;
        _btnMap.alpha = 0.3f;
    }
//    _isOpenMenu = true;
//    [self touchesBtnMenu:nil];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    _btnMenu.alpha = 0.7f;
    
    if(_isOpenMenu) {
        _btnSearch.alpha  = 0.7f;
        _btnSetting.alpha = 0.7f;
        _btnMap.alpha = 0.7f;
    }
}

@end
