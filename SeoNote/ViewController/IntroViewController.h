//
//  IntroViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RQShineLabel.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntroViewController : UIViewController

@property (strong, nonatomic) UIStoryboard *sb;

@property (weak, nonatomic) IBOutlet UIImageView *introImage;
@property (strong, nonatomic) IBOutlet RQShineLabel *shineLabel;


@end

NS_ASSUME_NONNULL_END
