//
//  SearchViewController.h
//  SeoNote
//
//  Created by 신서희 on 27/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ConstantsDB.h"
#import "NoteMD.h"
#import "NoteDBMD.h"

NS_ASSUME_NONNULL_BEGIN

@interface SearchViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (strong, nonatomic) UIStoryboard *sb;
@property (strong, nonatomic) NSString *tempPw;

@property (weak, nonatomic) IBOutlet UITableView *searchTableView;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) NSArray<NoteDBMD*> *data;
@property (strong, nonatomic) NSMutableArray<NoteDBMD*> *filteredData;

@property (strong, nonatomic) ConstantsDB *db;

@end

NS_ASSUME_NONNULL_END
