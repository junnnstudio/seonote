//
//  SearchViewController.m
//  SeoNote
//
//  Created by 신서희 on 27/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "SearchViewController.h"
#import "NoteDetailViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _db = [[ConstantsDB alloc] initDatabase];
    
    _filteredData = [[NSMutableArray alloc] init]; //이곳이 검색된 데이터들이 들어갈 배열
}

- (void) viewWillAppear:(BOOL)animated{
    //암호화 설정 유무 NSUserDefaults 를 이용함 (내부 DB같은 것)
    _tempPw = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    
    if(_tempPw != nil){
        _data = [[_db selectSearchTextNote] mutableCopy]; //폴더에 속하지 않거나, 비밀설정이 되지않은 폴더에 속한 노트만 불러온 리스트
    }else{
        _data = [[_db selectNoteAll] mutableCopy]; //비밀번호가 걸려있지않기 때문에 모든 노트를 가져옴.
    }
    
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _filteredData.count; //검색된 데이터 갯수
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *noteCell   = @"identifierNoteCell";
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:noteCell];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row < _filteredData.count){
        NoteDBMD *tempNoteDB = [_filteredData objectAtIndex:indexPath.row];
        cell.textLabel.text = tempNoteDB.m_contents;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    }
    
    return cell;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    [_filteredData removeAllObjects];
    
    if (searchText.length != 0) {
        
        for(int i=0; i<[_data count]; i++) {
            if([[_data objectAtIndex:i].m_contents containsString:searchText]) {
                [_filteredData addObject:[_data objectAtIndex:i]];
            }
        }
    }
    [_searchTableView reloadData];
}


#pragma mark - uitableviewdelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section == 0) {
        NoteDBMD *tempNoteDb = [_filteredData objectAtIndex:indexPath.row];
        
        NoteDetailViewController *noteDetailViewController = [_sb instantiateViewControllerWithIdentifier:@"detailStroyboard"];
        
        noteDetailViewController.getNo = tempNoteDb.m_noteID;
        [self presentViewController:noteDetailViewController animated:YES completion:nil];
        
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

@end
