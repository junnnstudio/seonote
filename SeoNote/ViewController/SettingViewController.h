//
//  SettingViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ConstantsDB.h"
#import "FolderMD.h"
#import "FolderDBMD.h"
#import "NoteMD.h"
#import "NoteDBMD.h"

NS_ASSUME_NONNULL_BEGIN

@interface SettingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIStoryboard *sb;

@property (strong, nonatomic) NSMutableArray *arrFolder;
@property (strong, nonatomic) NSMutableArray *arrNote;

@property (weak, nonatomic) IBOutlet UITableView *settingTableView;

@property (strong, nonatomic) ConstantsDB *db;

- (IBAction)touchesBtnClose:(id)sender;


@end

NS_ASSUME_NONNULL_END
