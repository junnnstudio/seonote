//
//  NoteAddViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "NoteAddViewController.h"

#import "NoteMD.h"
#import "NoteDBMD.h"
#import "AttachMD.h"
#import "AttachDBMD.h"
#import "FolderDBMD.h"

#import "ConstantsDB.h"

#import "AttachFileTableViewCell.h"

#import "GenenalFun.h"


@interface NoteAddViewController ()

@end


@implementation NoteAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor whiteColor]; 
    }
    
    _db = [[ConstantsDB alloc] initDatabase];
    
    _tempImageView = [[UIImageView alloc] init];
    _tempImageView.frame = CGRectMake(0, 0, 30, 30);
    _tempImageView.backgroundColor = [UIColor blueColor];
    
    _tempImageArray = [[NSMutableArray alloc] init];
    
    _tempImage = [[UIImagePickerController alloc] init];
    _tempImage.delegate = self;
    
    [_addTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    // NSNotificationCennter 에 키보드 상태를 등록해 준다. (호출할 메소드)
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAnimate:) name:UIKeyboardWillShowNotification object:nil];
    
    FolderDBMD *folderDBMD = [_db selectFolderByfolderID:_getFolderNo];
    if(folderDBMD != nil) {
        _naviBar.tintColor = RGB(folderDBMD.m_red, folderDBMD.m_green, folderDBMD.m_blue);
        _toolBar.tintColor = RGB(folderDBMD.m_red, folderDBMD.m_green, folderDBMD.m_blue);
    }
    
    _latitude  = 0.0F;
    _longitude = 0.0F;
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
    [_locationManager startUpdatingLocation];
    
    [_locationManager requestWhenInUseAuthorization];
    [_locationManager requestAlwaysAuthorization];
    
    _lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0,_addTextView.frame.size.width - 10.0, 34.0)];
    
    [_lbl setText:@"Please enter your text."];
    [_lbl setBackgroundColor:[UIColor clearColor]];
    [_lbl setTextColor:[UIColor lightGrayColor]];
    
    [_addTextView addSubview:_lbl];
    
}

- (void)textViewDidEndEditing:(UITextView *) textView {
    if (![_addTextView hasText]) {
        _lbl.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    if(![_addTextView hasText]) {
        _lbl.hidden = NO;
    }
    else {
        _lbl.hidden = YES;
    }
}



- (void)viewWillAppear:(BOOL)animated{
    [_attachTableView reloadData]; //첨부파일 테이블뷰 리로드
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    // ViewController를 종료 할때 등록한 키보드 상태를 삭제 해 준다. 
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_locationManager stopUpdatingLocation];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if(![self.addTextView isFirstResponder]) {
            [self.addTextView becomeFirstResponder];
        }
    });
    
}



// 등록한 메소드를 호출 해서 키보드 상태가 Show 인지 Hiden 인지에 따라 처리
- (void)keyboardAnimate:(NSNotification *)notification {
    if([notification name] == UIKeyboardWillShowNotification) {
        float keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        
        CGRect frame  = self.toolBar.frame;
        CGRect frameT = self.attachTableView.frame;
        
        frame.origin.y  = self.view.frame.size.height - keyboardHeight - _toolBar.frame.size.height;
        frameT.origin.y = self.view.frame.size.height - keyboardHeight;
        
        [UIView animateWithDuration:0.1F animations:^{
            self.toolBar.frame = frame;
            self.attachTableView.frame = frameT;
        }completion:^(BOOL finished) {
            [self resizeAddTextView:keyboardHeight];
        }];
    }else {
       
    }
}

- (void)resizeAddTextView:(float)keyboardHeight {
    
    CGRect frame = self.addTextView.frame;
    frame.size.height = self.view.frame.size.height - keyboardHeight - _toolBar.frame.size.height - frame.origin.y;
    
    NSLog(@"%lf", frame.size.height);
    
    [UIView animateWithDuration:0.01F animations:^{
        self.addTextView.frame = frame;
    }completion:^(BOOL finished) {
        //
    }];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _tempImageArray.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *imageCell   = @"identifierNoteCell";
    
    AttachFileTableViewCell *cell = (AttachFileTableViewCell*)[tableView dequeueReusableCellWithIdentifier:imageCell];
    if(cell == nil) {
        cell = [[AttachFileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:imageCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row < [_tempImageArray count]){
        
        NSString *fileName = [[_tempImageArray objectAtIndex:indexPath.row] lastPathComponent];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", _db.documentsDir, fileName];
        
        cell.lbfileName.text = fileName;
        
        NSData *imgData = [NSData dataWithContentsOfFile:filePath];
        UIImage *img = [[UIImage alloc] initWithData:imgData];
        cell.ivAttachFile.image = img;
        //cell.ivAttachFile.image = [UIImage imageNamed:[_tempImageArray objectAtIndex:indexPath.row]];
        cell.ivAttachFile.contentMode = UIViewContentModeScaleAspectFit; //이미지가 바뀌지않음..
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //오->왼으로 슬라이드시
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 메모 삭제
        NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSError *error = nil;
        NSString *fileName = [[_tempImageArray objectAtIndex:indexPath.row] lastPathComponent];
        [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&error];
        
        [_tempImageArray removeObjectAtIndex:indexPath.row];
        
        [_attachTableView reloadData];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)touchesBtnClose:(id)sender {
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    
    for(NSString *tempFile in _tempImageArray){
        NSString *fileName = [tempFile lastPathComponent];
        [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&error];
    }
    
    [_tempImageArray removeAllObjects];
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)touchesPhotoBtn:(id)sender {
    
    _tempImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_tempImage animated:YES completion:nil];
    
}

- (IBAction)touchesCameraBtn:(id)sender {
    
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
    {
        _tempImage.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:_tempImage animated:YES completion:nil];
    }else{
        //alert 카메라 안나온다는거 추가
        UIAlertController *cameraAlert = [UIAlertController alertControllerWithTitle:nil message:@"Camera error" preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *doneOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [cameraAlert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [cameraAlert addAction:doneOk];
        [self presentViewController:cameraAlert animated:YES completion:nil];
    }
  
    
}

- (IBAction)touchesSaveBtn:(id)sender {
    
    NSString *contents = _addTextView.text==nil?@"":_addTextView.text;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    NSString *noteDate = [formatter stringFromDate:[NSDate date]]; //노트 생성날짜
    
    NSInteger folderId = 0;
    
    if(_getFolderNo != 0) {
        folderId = _getFolderNo;
    }
    
    NoteDBMD *noteDBMD = [[NoteDBMD alloc] initWithNoteMD:-1 m_folderID:folderId m_contents:contents m_create_date:noteDate lat:_latitude lot:_longitude];
    [_db insertNote:noteDBMD]; //노트 저장
    
    NoteDBMD *tmpNote = [_db selectNoteLimitOne]; //저장한 노트를 다시 가져온다. id값 가져와야함.
    
    for(NSString *tempAttach in _tempImageArray){ //선택한 사진들 배열
        NSInteger type = 0;
        
        NSString *fileName = [tempAttach lastPathComponent];
        
        CFStringRef fileExtension = (CFStringRef) CFBridgingRetain(tempAttach.lastPathComponent.pathExtension); //확장자
        CFStringRef fileUTI =  UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL); //파일 타입.
        
        if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
            NSLog(@"It's an image");
            type = 0;
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeMovie)){
            NSLog(@"It's a movie");
            type = 1;
        }
        else if(UTTypeConformsTo(fileUTI, kUTTypeAudio)){
            NSLog(@"It's a audio");
            type = 2;
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeText)){
            NSLog(@"It's text");
            type = 3;
        }
        
        CFRelease(fileUTI);
        
        AttachDBMD *attachDBMD = [[AttachDBMD alloc] initWithAttachMD:-1 m_noteID:tmpNote.m_noteID m_attach_type:type m_file_name:fileName]; //첨부파일 저장.
        [_db insertAttach:attachDBMD];
    }
    _addTextView.text = @"";
    
    [self dismissViewControllerAnimated:TRUE completion:nil]; //저장 후 리스트 화면으로 이동 
}

- (IBAction)touchesBtnShowTable:(id)sender {
    [_addTextView resignFirstResponder];
}


#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}
    
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
}




#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
//    _writeImageView.image = image;
    //app Documents 경로
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *strFindUniqueSavePath = [documentsPath objectAtIndex:0];
    strFindUniqueSavePath = [strFindUniqueSavePath stringByAppendingString:@"/"] ;
    
    NSString *tempSavePath = @"";
    
    UIGraphicsBeginImageContext(image.size);
    
    _strImageName = [self findUniqueSavePath:strFindUniqueSavePath];
    [_tempImageArray addObject:_strImageName]; //임시 첨부파일 이미지를 배열에 넣는다. (경로)
    NSLog(@"%@", _strImageName);
    
    NSArray *tempImageArr = [_strImageName componentsSeparatedByString:@"/"];
    _imagePath = [tempImageArr lastObject];
    
    tempSavePath = [strFindUniqueSavePath stringByAppendingString:_imagePath];
    
    [UIImageJPEGRepresentation(image, 1.0f) writeToFile:tempSavePath atomically:YES];
    UIGraphicsEndImageContext();
    
    
    // 지도용 이미지를 저장한다.
    UIImage *tempImage = [GenenalFun imageWithImage:image scaledResize:MAP_IMAGE_SIZE];
    UIGraphicsBeginImageContext(tempImage.size);
    NSArray *tempA = [_imagePath componentsSeparatedByString:@"."];
    
    tempSavePath = [strFindUniqueSavePath stringByAppendingString:[NSString stringWithFormat:@"%@_map.jpg", [tempA firstObject]]];
    [UIImageJPEGRepresentation(tempImage, 1.0f) writeToFile:tempSavePath atomically:YES];
    UIGraphicsEndImageContext();
    // end of 지도용 이미지 저장

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)findUniqueSavePath:(NSString *)rootPath {
    int i = 0;
    NSString *path;
    
    do {
        path = [NSString stringWithFormat:@"%@/photo_%04d.jpg",rootPath, i++];
    } while ([[NSFileManager defaultManager] fileExistsAtPath:path]);
    
    return path;
}

// 취소할 경우
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    CLLocation *location = [locations lastObject];
    NSDate *eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if (fabs(howRecent) < 15.0) {
        _latitude  = location.coordinate.latitude;
        _longitude = location.coordinate.longitude;
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {

}


@end
