//
//  FolderAddViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "FolderAddViewController.h"

#import "FolderMD.h"
#import "FolderDBMD.h"
#import "ConstantsDB.h"
#import "SQL.h"


#define BTN_FOLDER_TAG   1000

@interface FolderAddViewController ()

@end

@implementation FolderAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _buttonArray = [[NSArray alloc] initWithObjects:_btn1,_btn2,_btn3,_btn4,_btn5,_btn6,_btn7,_btn8,_btn9,_btn10,_btn11,_btn12,nil];
    
    _colorArray = [[NSArray alloc] initWithObjects:GREEN_ONE_RGB, GREEN_TWO_RGB, BLUE_RGB, DARK_BLUE_RGB, PURPLE_RGB, YELLOW_RGB, ORENGE_ONE_RGB, ORENGE_TWO_RGB, RED_ONE_RGB, RED_TWO_RGB, BLACK_RGB, GRAY_RGB,nil];
    
    //12개 폴더 생성 (폴더색)
    for (int i = 0; i < _colorArray.count-1; i++) {
        UIButton *tempBtn = [_buttonArray objectAtIndex:i];
        tempBtn.tag = BTN_FOLDER_TAG + i;
    }
    
    _db = [[ConstantsDB alloc] initDatabase];
    [_folderAddTextField becomeFirstResponder];
    
    _imageView = [[UIImageView alloc] init];
    _imageView.frame = CGRectMake(0, 0, 40, 40);
    _imageView.image = [UIImage imageNamed:@"check"];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)touchesBtnClose:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil]; //현재창을 닫는다.
    
}

- (IBAction)tapGestureAction:(id)sender {
    
    if([_folderAddTextField isFirstResponder]){
        [_folderAddTextField resignFirstResponder];
    }else{
        [_folderAddTextField becomeFirstResponder];
    }
    
}

- (IBAction)touchesBtnFolder:(id)sender {
    [self performSelector:@selector(doHighlight:) withObject:sender afterDelay:0];

}

- (IBAction)touchesBtnSave:(id)sender {
    
    [self performSelector:@selector(folderSave:) withObject:sender afterDelay:0];
}

- (void)doHighlight:(UIButton*)button {
    _selectedButtonTag = button.tag - BTN_FOLDER_TAG;
    [button addSubview:_imageView]; //이거쓰기
}

- (void)folderSave:(UIButton*)button{
    
    UIColor *tempColor = [_colorArray objectAtIndex:_selectedButtonTag];
    const CGFloat* components = CGColorGetComponents(tempColor.CGColor);
    CGFloat red = components[0]*255.0;
    CGFloat green = components[1]*255.0;
    CGFloat blue = components[2]*255.0;
    
    FolderDBMD *folderDBMD = [[FolderDBMD alloc] initWithFolderMD:-1 m_name:_folderAddTextField.text m_red:red m_green:green m_blue:blue m_hidden_yn:@"N"];
    [_db insertFolder:folderDBMD];
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
    
}
@end
