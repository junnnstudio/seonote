//
//  FolderAddViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FolderMD.h"

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN

@interface FolderAddViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *folderAddTextField;

@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn4;
@property (weak, nonatomic) IBOutlet UIButton *btn5;
@property (weak, nonatomic) IBOutlet UIButton *btn6;
@property (weak, nonatomic) IBOutlet UIButton *btn7;
@property (weak, nonatomic) IBOutlet UIButton *btn8;
@property (weak, nonatomic) IBOutlet UIButton *btn9;
@property (weak, nonatomic) IBOutlet UIButton *btn10;
@property (weak, nonatomic) IBOutlet UIButton *btn11;
@property (weak, nonatomic) IBOutlet UIButton *btn12;

@property (nonatomic) UIImageView *imageView;
@property (nonatomic) NSArray *colorArray;
@property (nonatomic) NSArray *buttonArray;
@property (assign, nonatomic) NSInteger selectedButtonTag;

@property (strong, nonatomic) ConstantsDB *db;

- (IBAction)tapGestureAction:(id)sender;
- (IBAction)touchesBtnFolder:(id)sender;
- (IBAction)touchesBtnSave:(id)sender;

@end

NS_ASSUME_NONNULL_END
