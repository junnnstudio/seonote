//
//  SettingHiddenViewController.h
//  SeoNote
//
//  Created by 신서희 on 28/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FolderMD.h"
#import "FolderDBMD.h"

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN

@interface SettingHiddenViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIStoryboard *sb;
@property (strong, nonatomic) NSMutableArray *arrFolder;
@property (strong, nonatomic) ConstantsDB *db;
@property (strong, nonatomic) NSString *tempPw;

@property (strong, nonatomic) UISwitch *hiddenUseSwitch;
@property (weak, nonatomic) IBOutlet UITableView *settingHiddenTableView;

- (IBAction)touchesCloseBtn:(id)sender;
- (IBAction)touchesSaveBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
