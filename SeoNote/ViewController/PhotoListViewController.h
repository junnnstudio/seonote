//
//  PhotoListViewController.h
//  SeoNote
//
//  Created by 신서희 on 21/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN


@interface PhotoListViewController : UIViewController <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *photoScrollView;
@property (strong, nonatomic) NSMutableArray *arrPhoto;
@property (nonatomic) NSInteger getPhotoNo;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigationItem;

@property (strong, nonatomic) ConstantsDB *db;

- (IBAction)touchesCloseBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
