//
//  FolderMemoListViewController.h
//  SeoNote
//
//  Created by 신서희 on 12/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteMD.h"
#import "NoteDBMD.h"

#import "FolderDBMD.h"

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN

@interface FolderMemoListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) UIStoryboard *sb;
@property (weak, nonatomic) IBOutlet UITableView *noteTableView;

@property (weak, nonatomic) IBOutlet UINavigationBar *naviBar;


@property (strong, nonatomic) ConstantsDB *db;
@property (nonatomic) NSInteger getFolderNo;

@property (strong, nonatomic) NSMutableArray *arrNote;
@property (strong, nonatomic) FolderDBMD *folderDBMD;

@end

NS_ASSUME_NONNULL_END
