//
//  PasswordViewController.m
//  SeoNote
//
//  Created by 신서희 on 27/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "PasswordViewController.h"
#import "SettingHiddenViewController.h"

#define MAXLENGTH 1

@interface PasswordViewController ()

@end

@implementation PasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _passwdConfirmView.hidden = true;
    _passwdConfirmLabel.hidden = true;
    
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    [_passwordOne becomeFirstResponder];
    
    _tempPw = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *tempPasswd1 = [NSString stringWithFormat:@"%@%@%@%@", _passwordOne.text,_passwordTwo.text, _passwordThree.text, _passwordFour.text ];
    
    NSString *tempPasswd2 = [NSString stringWithFormat:@"%@%@%@%@", _passwdConfirmOne.text,_passwdConfirmTwo.text, _passwdConfirmThree.text, _passwdConfirmFour.text ];
    
    if(_tempPw != nil){
        _passwdConfirmView.hidden = true;
        if([_tempPw isEqualToString:tempPasswd1] && tempPasswd1.length == 4){
            _passwdConfirmLabel.hidden = true;
            [self viewOpen:true];
        }else if(![_tempPw isEqualToString:tempPasswd1] && tempPasswd1.length == 4){
            _passwdConfirmLabel.hidden = false;
            _passwordOne.text = nil;
            _passwordTwo.text = nil;
            _passwordThree.text = nil;
            _passwordFour.text = nil;
        }
    }else{
        if(tempPasswd1.length == 4 && tempPasswd2.length == 4){
            if(![tempPasswd1 isEqualToString:tempPasswd2]){
                _passwdConfirmLabel.hidden = false;
            }else{
                _passwdConfirmLabel.hidden = true;
                
                [[NSUserDefaults standardUserDefaults] setObject:tempPasswd1 forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self viewOpen:false];
            }
        }else{
            _passwdConfirmLabel.hidden = true;
        }
    }
}

- (void)viewOpen:(BOOL)is {
    if(is) {
        UIViewController *tempView = self.presentingViewController;
        
        [self dismissViewControllerAnimated:TRUE completion:^{
            self->_sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SettingHiddenViewController *settingHiddenViewController = [self->_sb instantiateViewControllerWithIdentifier:@"settinghiddenviewcontroller"];
            [settingHiddenViewController setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [tempView presentViewController:settingHiddenViewController animated:TRUE completion:nil];
        }];
    }else {
        [self dismissViewControllerAnimated:TRUE completion:nil];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSInteger length = [textField.text length] ;
    NSInteger length = [string length] ;
    if (length >= MAXLENGTH && ![string isEqualToString:@""]) {
        textField.text = [string substringToIndex:MAXLENGTH];
        
        [textField resignFirstResponder];
        
        if(textField.tag == 1){
            if (textField == _passwordOne) {
                [_passwordTwo becomeFirstResponder];
            }else if(textField == _passwordTwo){
                [_passwordThree becomeFirstResponder];
            }else if(textField == _passwordThree){
                [_passwordFour becomeFirstResponder];
            }else{
                if(_tempPw == nil){
                    _passwdConfirmView.hidden = false;
                    [_passwdConfirmOne becomeFirstResponder];
                }else{
                    [_passwordOne becomeFirstResponder];
                }
            }
        }else{
            if (textField == _passwdConfirmOne) {
                [_passwdConfirmTwo becomeFirstResponder];
            }else if(textField == _passwdConfirmTwo){
                [_passwdConfirmThree becomeFirstResponder];
            }else if(textField == _passwdConfirmThree){
                [_passwdConfirmFour becomeFirstResponder];
            }
        }
        return NO;
    }
    return YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)touchesCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
