//
//  MainViewController.h
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ConstantsDB.h"
#import "FolderMD.h"
#import "FolderDBMD.h"
#import "NoteMD.h"
#import "NoteDBMD.h"


NS_ASSUME_NONNULL_BEGIN

@interface MainViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (assign, nonatomic) BOOL isOpenMenu;
@property (strong, nonatomic) NSString *tempPw;

@property (strong, nonatomic) UIStoryboard *sb;

@property (strong, nonatomic) NSMutableArray *arrFolder;
@property (strong, nonatomic) NSMutableArray *arrNote;

@property (weak, nonatomic) IBOutlet UITableView *mainTableView;

@property (strong, nonatomic) UIButton *btnMenu;
@property (strong, nonatomic) UIButton *btnSearch;
@property (strong, nonatomic) UIButton *btnSetting;
@property (strong, nonatomic) UIButton *btnMap;

@property (nonatomic) NSMutableArray<FolderDBMD*> *folderDBList;

@property (strong, nonatomic) NSArray *arrColorTheme;
@property (strong, nonatomic) NSString *strTheme;
@property (strong, nonatomic) ConstantsDB *db;

@end

NS_ASSUME_NONNULL_END
