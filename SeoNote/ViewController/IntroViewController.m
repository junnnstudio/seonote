//
//  IntroViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 21/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "IntroViewController.h"

@interface IntroViewController ()

@end

@implementation IntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *arrTemp = [[NSArray alloc] initWithObjects:@"일상을\n\n기록 하세요\n\n서노트는 간편합니다.", @"봄 여름 가을 겨울\n\n언제나 서노\n트\n가\n\n함\n께\n\n합\n니\n다\n.", @"슬\n플\n때\n나\n\n즐\n거\n울\n때\n\n그\n\n순\n간\n을\n\n노\n트\n해\n\n보\n세\n요\n.", @"클\n라\n우\n드\n\n백\n업\n\n그\n런\n기\n능\n\n절\n때\n\n없\n습\n니\n다\n.\n\n오\n직\n\n디\n바\n이\n스\n에\n서\n만\n\n존\n재\n\n합\n니\n다\n. ", @"비\n빌\n번\n호\n\n설\n정\n으\n로\n\n누\n구\n도\n\n알\n지\n\n못\n하\n게\n\n노\n트\n를\n\n보\n관\n하\n세\n요", nil];
    
    
    //intro 이미지 위치 설정
    CGRect frame = _introImage.frame;
    frame.origin.y = 100;
    
//    int temp = arc4random_uniform((uint32_t)[arrTemp count]);
////
//    self.shineLabel = ({
//        RQShineLabel *label = [[RQShineLabel alloc] init];
//        label.frame = self.shineLabel.frame;
//        label.numberOfLines = 0;
//        label.textColor = [UIColor blackColor];
//        label.text = [arrTemp objectAtIndex:temp];
//        label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
//        label.backgroundColor = [UIColor clearColor];
//        [label sizeToFit];
//        label;
//    });
//    [self.view addSubview:self.shineLabel];
//
//    [self.shineLabel shine];
    
    
    [UIView animateWithDuration:1.0F delay:1.5F options:UIViewAnimationOptionCurveEaseIn animations:^{
        self->_introImage.alpha = 0.2;
    } completion:^(BOOL finished) {
        //위에 애니메이션 완료 후 이 코드 실행 block code
        self->_sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *navigationController = [self->_sb instantiateViewControllerWithIdentifier:@"mainviewnavigationcontroller"];
        //main view 로 페이지 이동

        [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        //이동할때 스타일 4개
        //UIModalTransitionStyleCoverVertical  UIModalTransitionStyleFlipHorizontal  UIModalTransitionStyleCrossDissolve, UIModalTransitionStylePartialCurl
        [self presentViewController:navigationController animated:true completion:nil];
    }];
    
    
    
//    [UIView animateWithDuration:1.5f animations:^{
//        //이미지 깜빡거리는 애니메이션.
//        self->_introImage.alpha = 1;
//    } completion:^(BOOL finished) {
//        //위에 애니메이션 완료 후 이 코드 실행 block code
//        self->_sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        UINavigationController *navigationController = [self->_sb instantiateViewControllerWithIdentifier:@"mainviewnavigationcontroller"];
//        //main view 로 페이지 이동
//
//        [navigationController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//        //이동할때 스타일 4개
//        //UIModalTransitionStyleCoverVertical  UIModalTransitionStyleFlipHorizontal  UIModalTransitionStyleCrossDissolve, UIModalTransitionStylePartialCurl
//        [self presentViewController:navigationController animated:true completion:nil];
//    }];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}




@end
