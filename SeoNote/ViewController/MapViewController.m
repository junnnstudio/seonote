//
//  MapViewController.m
//  SeoNote
//
//  Created by hjunnn kim on 01/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "MapViewController.h"

#import "MapContentMD.h"
#import "NoteDetailViewController.h"

#import "FBClusteringManager.h"

#define pLabelTitle    550
#define pLabelCnt    560

@interface MapViewController ()

@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _db = [[ConstantsDB alloc] initDatabase];
    _arrMapContent = [[_db selectMapContent] mutableCopy];
    
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _strFindUniqueSavePath = [documentsPath objectAtIndex:0];
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    [self.locationManager startUpdatingLocation];
    
    [self myLocation];
    [self makeMapAnnotation];
    
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager requestAlwaysAuthorization];
    
    self.clusteringManager = [[FBClusteringManager alloc] init];
}

- (void)viewWillDisappear:(BOOL)animated {
//    [[self.view viewWithTag:20] removeFromSuperview];
//    [[self.view viewWithTag:42] removeFromSuperview];
    [super viewWillDisappear:animated];
//
    [_locationManager stopUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated{
//    [[self.view viewWithTag:20] removeFromSuperview];
//    [self makeMapAnnotation];
    [super viewWillAppear: animated];
//    [self myLocation];
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 800, 800);
    
    [_map setRegion:[_map regionThatFits:region] animated:YES];
}


- (void)myLocation {
    [self.map setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
}



// custom annotation
- (void)makeMapAnnotation {
//    MKMapRect zoomRect = MKMapRectNull;
    
    for(int i=0; i<[_arrMapContent count]; i++) {
        MapContentMD *md = [_arrMapContent objectAtIndex:i];
        
        CustomAnnotation *cusA = [[CustomAnnotation alloc] init];
        CLLocationCoordinate2D coordinate;
        
        coordinate.latitude  = md.mLatitude;
        coordinate.longitude = md.mLongitude;
        
        cusA.coordinate = coordinate;
        
        cusA.title = md.mCreateDate;
        cusA.noteID = md.mNoteID;

        cusA.imgName  = md.mFileName;
        
        [_map addAnnotation:cusA];
        
        [_arrFBClusteringManager addObject:cusA];
        
//        CLLocationCoordinate2D fbCoordinate;
//
//        fbCoordinate.latitude  = md.mLatitude;
//        fbCoordinate.longitude = md.mLongitude;
//
//        fbC.coordinate = fbCoordinate;
//
//        fbC.title = md.mCreateDate;
//        fbC.noteID = md.mNoteID;
        
//        fbC.imgName  = md.mFileName;
        
       // [_arrFBClusteringManager addObject:fbC];
        
//        MKMapPoint annotationPoint = MKMapPointForCoordinate(coordinate);
//        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0, 0);
//        if (MKMapRectIsNull(zoomRect)) {
//            zoomRect = pointRect;
//        } else {
//            zoomRect = MKMapRectUnion(zoomRect, pointRect);
//        }
        
    }
    //[self.map setVisibleMapRect:zoomRect animated:YES];
//    [self.map setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(10, 10, 10, 10) animated:YES];
    self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:_arrFBClusteringManager];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    
    CLLocation *location = [locations lastObject];
    NSDate *eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    
    if(fabs(howRecent) < 15.0) {
        // If the event is recent, do something with it.
        NSLog(@"latitude %+.6f, longitude %+.6f\n", location.coordinate.latitude, location.coordinate.longitude);
    }
}


- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSLog(@"locationManage didFailWithError");
}



#pragma mark - MKMapViewDelegate
// Line 안씀
//- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
//
//    MKPolylineRenderer* lineView = [[MKPolylineRenderer alloc] initWithPolyline:_polyLine];
//    lineView.strokeColor = [UIColor blueColor];
//    lineView.lineWidth = 3;
//    return lineView;
//
//}

// Custom Pin
//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
//    MKAnnotationView *pinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"customPinAnnotationView"];
//
//    pinView.canShowCallout = true;
//    [pinView setTintColor:[UIColor blueColor]];
//    pinView.draggable = true;
//    pinView.accessibilityLabel = @"TEST";
//
//    UIButton *tempBtn = [UIButton buttonWithType:UIButtonTypeInfoLight];
//    pinView.rightCalloutAccessoryView = tempBtn;
//    pinView.image = [UIImage imageNamed:@"folder_yellow"];
//
//    return pinView;


//    NSString *customerAnnotationIdentifier = @"customAnn";
//
//    CustomAnnotation *customerAnnotation = (CustomAnnotation*)annotation;
//    customerAnnotation.title = @"Name";
//
//    MKAnnotationView *pinView = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:customerAnnotationIdentifier];
//    if(!pinView) {
//        pinView = [[MKAnnotationView alloc] initWithAnnotation:customerAnnotation reuseIdentifier:customerAnnotationIdentifier];
//        pinView.canShowCallout = YES;
//        pinView.calloutOffset = CGPointMake(0, 0);
//
//    }else {
//        pinView.annotation = annotation;
//    }
//
//    UIImage *markerIcon = [UIImage imageNamed:@"folder_yellow"];
//    pinView.image = markerIcon;
//
//    // Add a detail disclosure button to the callout.
//    UIButton* rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//    pinView.rightCalloutAccessoryView = rightButton;
//
//    return pinView;
//}


- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if([annotation isKindOfClass:[CustomAnnotation class]]) {
        static NSString *reuseId = @"customAnn";
        
        MKAnnotationView *aView = [aMapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        //CustomAnnotationView *aView = [aMapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
        
        if(aView == nil) {
            aView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
            UIImage *pinImage    = [UIImage imageNamed:@"pin-green.png"];
            [aView setImage:pinImage];
            aView.canShowCallout = YES;
            
//            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//            aView.rightCalloutAccessoryView = rightButton;
        }
        
        CustomAnnotation *customAnnotation = (CustomAnnotation *)annotation;
        
        // MKAnnotationView 에 붙인다.
        NSArray *tempA = [customAnnotation.imgName componentsSeparatedByString:@"."];
        NSString *tempSavePath = [_strFindUniqueSavePath stringByAppendingString:[NSString stringWithFormat:@"/%@_map.jpg", [tempA firstObject]]];
        NSData *imgData = [NSData dataWithContentsOfFile:tempSavePath];
        UIImage *img    = [[UIImage alloc] initWithData:imgData];
        
        aView.image = img;
        aView.annotation = annotation;
        aView.layer.cornerRadius = 2;
//        aView.layer.borderWidth  = 2;
//        aView.layer.borderColor  = [UIColor whiteColor].CGColor;
//        aView.layer.zPosition = -1;
        
        [[aView viewWithTag:pLabelCnt] removeFromSuperview];
        [[aView viewWithTag:pLabelTitle] removeFromSuperview];
        
        UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(aView.layer.frame.size.width-aView.image.size.width , aView.layer.frame.size.height, aView.image.size.width, 30)];
        nameLbl.tag = pLabelTitle;    //set tag on it so we can easily find it later
        nameLbl.textColor = [UIColor blackColor];
        nameLbl.backgroundColor = [UIColor whiteColor];
        nameLbl.textAlignment = NSTextAlignmentCenter;
        nameLbl.text = customAnnotation.title;
        nameLbl.font = [nameLbl.font fontWithSize:9];
        [aView addSubview:nameLbl];
        
//        UILabel *nameLbl2 = [[UILabel alloc] initWithFrame:CGRectMake(aView.layer.frame.size.width-15, -15, 30, 30)];
//        nameLbl2.tag = 20;
//        nameLbl2.layer.cornerRadius = 9;
//        nameLbl2.layer.borderWidth  = 1;
//        nameLbl2.layer.borderColor  = [UIColor blackColor].CGColor;
//        nameLbl2.textColor = [UIColor blackColor];
//        nameLbl2.backgroundColor = [UIColor whiteColor];
//        nameLbl2.textAlignment = NSTextAlignmentCenter;
        
        
//        NSString *tempText = [NSString stringWithFormat:@"%d", num];
//        nameLbl.text = tempText;
//        [aView addSubview:nameLbl2];
        
        
        return aView;
    }
    return nil;
}

//
//- (MKAnnotationView *)mapView:(MKMapView *)aMapView viewForAnnotation:(id<MKAnnotation>)annotation {
//    if([annotation isKindOfClass:[CustomAnnotation class]]) {
//        static NSString *reuseId = @"customAnn";
//
//        MKAnnotationView *aView = [aMapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
//
//        if(aView == nil) {
//            aView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
//            UIImage *pinImage    = [UIImage imageNamed:@"pin-green.png"];
//            [aView setImage:pinImage];
//            aView.canShowCallout = YES;
////            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
////            aView.rightCalloutAccessoryView = rightButton;
//        }
//
//        CustomAnnotation *customAnnotation = (CustomAnnotation *)annotation;
//
//        // MKAnnotationView 에 붙인다.
//        NSArray *tempA = [customAnnotation.imgName componentsSeparatedByString:@"."];
//        NSString *tempSavePath = [_strFindUniqueSavePath stringByAppendingString:[NSString stringWithFormat:@"/%@_map.jpg", [tempA firstObject]]];
//        NSData *imgData = [NSData dataWithContentsOfFile:tempSavePath];
//        UIImage *img    = [[UIImage alloc] initWithData:imgData];
//
//        aView.image = img;
//        aView.annotation = annotation;
//
//        aView.layer.cornerRadius = 2;
//        aView.layer.borderWidth  = 2;
//        aView.layer.borderColor  = [UIColor whiteColor].CGColor;
//
//        return aView;
//    }
//    return nil;
//}


// Annotation rightCalloutAccessoryView 에 버튼을 달아 이동하는 방법
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    NSLog(@"calloutAccessoryControlTapped");
}


// 사진을 선택시 이동하는 방법
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    CustomAnnotation *temp = (CustomAnnotation*)view.annotation;
    if([temp isKindOfClass:[CustomAnnotation class]]) {
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NoteDetailViewController *noteDetailViewController = [sb instantiateViewControllerWithIdentifier:@"detailStroyboard"];
        noteDetailViewController.getNo = temp.noteID;
        
        [self presentViewController:noteDetailViewController animated:YES completion:nil];
    }
    
}

- (IBAction)touchesBtnMap:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

-(void)mapView:(MKMapView *)mapView_ regionWillChangeAnimated:(BOOL)animated
{
    NSMutableSet * coordSet = [[NSMutableSet alloc] init];
    int num = 1;
    MKAnnotationView * zz;
//    [[_map viewWithTag:pLabelCnt] removeFromSuperview];
    //for(id<MKAnnotation> an in mapView_.annotations)
    //for (int i=0; i<_map.annotations.count; i++)
    for(id<MKAnnotation> an in _map.annotations)
    {
        if([an isKindOfClass:[MKUserLocation class]])
            continue;
        
        CGPoint point = [mapView_ convertCoordinate:an.coordinate toPointToView:nil];
        CGPoint roundedPoint;
    
        roundedPoint.x = roundf(point.x/50)*10;
        roundedPoint.y = roundf(point.y/50)*10;
        
        NSValue * value = [NSValue valueWithCGPoint:roundedPoint];
        
        MKAnnotationView * av = [mapView_ viewForAnnotation:an];
        [[av viewWithTag:pLabelCnt] removeFromSuperview];
        
        if([coordSet containsObject:value])
        {
            av.hidden = YES;
            num++;
            
            UILabel *nameLbl = [[UILabel alloc] initWithFrame:CGRectMake(zz.layer.frame.size.width-15, -15, 30, 30)];
            nameLbl.tag = pLabelCnt;
            nameLbl.clipsToBounds = YES;
            nameLbl.layer.cornerRadius = nameLbl.frame.size.width/2;
//                nameLbl.layer.borderWidth  = 1;
//                nameLbl.layer.borderColor  = [UIColor blackColor].CGColor;
            nameLbl.textColor = [UIColor whiteColor];
            nameLbl.backgroundColor = RGB(73, 124, 245);
            nameLbl.textAlignment = NSTextAlignmentCenter;

            NSString *tempText = [NSString stringWithFormat:@"%d", num];
            nameLbl.text = tempText;
            [zz addSubview:nameLbl];
        }
        else
        {
            [coordSet addObject:value];
            av.hidden = NO;
            [[av viewWithTag:pLabelCnt] removeFromSuperview];
            
            if(num == 1){
                zz = av;
            }else{
                num = 1;
            }
        }
        
    }
    
    
    //[coordSet release];
}

//- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
//{
//    double mapBoundsWidth = mapView.bounds.size.width;
//    double mapRectWidth = mapView.visibleMapRect.size.width;
//    double scale  = mapBoundsWidth / mapRectWidth;
//    NSArray *annotationArray = [_clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
//    [_clusteringManager displayAnnotations:annotationArray onMapView:mapView];
//}


//
//- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated
//{
//    NSArray *annotations = [_map annotations];
//    CustomAnnotation *annotation = nil;
//
//    MKMapRect zoomRect = MKMapRectNull;
//
//    for (int i=0; i<[annotations count]; i++)
//    {
//        annotation = (CustomAnnotation*)[annotations objectAtIndex:i];
//        [mapView addAnnotation:annotation];
//
//        if (_map.region.span.latitudeDelta > .010)
//        {
//
////            [[_map viewForAnnotation:annotation] setHidden:YES];
//
////            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
////            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 100, 100);
////            if (MKMapRectIsNull(zoomRect)) {
////                zoomRect = pointRect;
////            } else {
////                zoomRect = MKMapRectUnion(zoomRect, pointRect);
////            }
////
////            [_map setVisibleMapRect:zoomRect animated:YES];
////            [[_map viewForAnnotation:annotation] setTintColor:[UIColor yellowColor]];
//
//            MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//            MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 100, 100);
//            if (MKMapRectIsNull(zoomRect)) {
//                zoomRect = pointRect;
//            } else {
//                zoomRect = MKMapRectUnion(zoomRect, pointRect);
//            }
//
//            [_map setVisibleMapRect:zoomRect animated:YES];
//            //[[_map viewForAnnotation:tempGroupAnnotation] setTintColor:[UIColor yellowColor]];
//
//
//        }
//        else {
//            [[_map viewForAnnotation:annotation] setHidden:NO];
//            [[_map viewForAnnotation:annotation] setTintColor:[UIColor blackColor]];
//        }
//    }
//}


@end
