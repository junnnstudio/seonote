//
//  NoteDetailViewController.h
//  SeoNote
//
//  Created by 신서희 on 28/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "NoteMD.h"
#import "NoteDBMD.h"
#import "AttachMD.h"
#import "AttachDBMD.h"

@class ConstantsDB;

NS_ASSUME_NONNULL_BEGIN

@interface NoteDetailViewController : UIViewController< UIImagePickerControllerDelegate, UITextViewDelegate, UITableViewDataSource,UINavigationControllerDelegate>

@property (strong, nonatomic) UIStoryboard *sb;

@property (strong, nonatomic) NSMutableArray *arrAttach;
@property (strong, nonatomic) NSMutableArray<AttachDBMD*> *tempArrAttach;

@property (assign, nonatomic) NSInteger folderNumber;


@property (nonatomic) NSInteger getNo;
@property (nonatomic) NoteDBMD *getNote;
@property (weak, nonatomic) IBOutlet UITextView *detailTextView;
@property (weak, nonatomic) IBOutlet UINavigationBar *naviBar;

@property (strong, nonatomic) NSString *imagePath;
@property (strong, nonatomic) ConstantsDB *db;
@property (nonatomic) UIImagePickerController *tempImage;

@property (weak, nonatomic) IBOutlet UITableView *attachTableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *layoutConstrainTableView;

@property (weak, nonatomic) IBOutlet UIToolbar *toolBar;
@property (nonatomic) UILabel *lbl;

@property (strong, nonatomic) NSString *strImageName;
@property (nonatomic) UIImageView *tempImageView;
@property (nonatomic) NSMutableArray *tempImageArray;

- (IBAction)touchesPhotoBtn:(id)sender;
- (IBAction)touchesCameraBtn:(id)sender;
- (IBAction)touchesSaveBtn:(id)sender;
- (IBAction)touchesBtnShowTable:(id)sender;
- (IBAction)touchesCloseBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
