//
//  PasswordViewController.h
//  SeoNote
//
//  Created by 신서희 on 27/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PasswordViewController : UIViewController<UITextFieldDelegate>

@property (strong, nonatomic) UIStoryboard *sb;
@property (strong, nonatomic) NSString *tempPw;

@property (weak, nonatomic) IBOutlet UITextField *passwordOne;
@property (weak, nonatomic) IBOutlet UITextField *passwordTwo;
@property (weak, nonatomic) IBOutlet UITextField *passwordThree;
@property (weak, nonatomic) IBOutlet UITextField *passwordFour;

@property (weak, nonatomic) IBOutlet UITextField *passwdConfirmOne;
@property (weak, nonatomic) IBOutlet UITextField *passwdConfirmTwo;
@property (weak, nonatomic) IBOutlet UITextField *passwdConfirmThree;
@property (weak, nonatomic) IBOutlet UITextField *passwdConfirmFour;

@property (weak, nonatomic) IBOutlet UIStackView *passwdConfirmView;
@property (weak, nonatomic) IBOutlet UILabel *passwdConfirmLabel;
- (IBAction)touchesCloseBtn:(id)sender;

@end

NS_ASSUME_NONNULL_END
