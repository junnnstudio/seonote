//
//  NoteDetailViewController.m
//  SeoNote
//
//  Created by 신서희 on 28/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "NoteDetailViewController.h"
#import "NoteMD.h"
#import "NoteDBMD.h"
#import "AttachMD.h"
#import "AttachDBMD.h"

#import "ConstantsDB.h"

#import "FolderDBMD.h"

#import "AttachFileTableViewCell.h"
#import "PhotoListViewController.h"

#import "GenenalFun.h"

@interface NoteDetailViewController ()

@end

@implementation NoteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = [UIColor whiteColor];
    }
    
    
    _db = [[ConstantsDB alloc] initDatabase];
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    _arrAttach = [[NSMutableArray alloc] init];
    _arrAttach = [[_db selectAttachNoteId:_getNo] mutableCopy];
    _tempArrAttach = [[NSMutableArray alloc] init];

    _getNote = [_db selectNoteId:_getNo];
    _detailTextView.text = _getNote.m_contents;
    
    _tempImageView = [[UIImageView alloc] init];
    _tempImageView.frame = CGRectMake(0, 0, 30, 30);
    _tempImageView.backgroundColor = [UIColor blueColor];
    
    _tempImageArray = [[NSMutableArray alloc] init];
    
    _tempImage = [[UIImagePickerController alloc] init];
    _tempImage.delegate = self;
    
    // 자동완성 강제 enable
    [_detailTextView setAutocorrectionType:UITextAutocorrectionTypeNo];
    
    // NSNotificationCennter 에 키보드 상태를 등록해 준다. (호출할 메소드) 키보드 on/off상태값
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardAnimate:) name:UIKeyboardWillShowNotification object:nil];
    
    
    FolderDBMD *folderDBMD = [_db selectFolderByfolderID:_folderNumber];
    if(folderDBMD != nil) {
        _naviBar.tintColor = RGB(folderDBMD.m_red, folderDBMD.m_green, folderDBMD.m_blue);
        _toolBar.tintColor = RGB(folderDBMD.m_red, folderDBMD.m_green, folderDBMD.m_blue);
    }
    
    _lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, 0.0,_detailTextView.frame.size.width - 10.0, 34.0)];
    
    [_lbl setText:@"Please enter your text."];
    [_lbl setBackgroundColor:[UIColor clearColor]];
    [_lbl setTextColor:[UIColor lightGrayColor]];
    
    [_lbl setHidden:YES];
    [_detailTextView addSubview:_lbl];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.01 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        if(![self.detailTextView isFirstResponder]) {
            [self.detailTextView becomeFirstResponder];
        }
    });
}

- (void)textViewDidEndEditing:(UITextView *) textView {
    if (![_detailTextView hasText]) {
        _lbl.hidden = NO;
    }
}

- (void) textViewDidChange:(UITextView *)textView {
    if(![_detailTextView hasText]) {
        _lbl.hidden = NO;
    }
    else {
        _lbl.hidden = YES;
    }
}




- (void)viewWillAppear:(BOOL)animated{
    [_attachTableView reloadData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    // ViewController를 종료 할때 등록한 키보드 상태를 삭제 해 준다.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}

// 등록한 메소드를 호출 해서 키보드 상태가 Show 인지 Hiden 인지에 따라 처리
- (void)keyboardAnimate:(NSNotification *)notification {
    if([notification name] == UIKeyboardWillShowNotification) {
        float keyboardHeight = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
        
        CGRect frame  = self.toolBar.frame;
        CGRect frameT = self.attachTableView.frame;
        
        frame.origin.y  = self.view.frame.size.height - keyboardHeight - _toolBar.frame.size.height;
        frameT.origin.y = self.view.frame.size.height - keyboardHeight;

        [UIView animateWithDuration:0.1F animations:^{
            self.toolBar.frame = frame;
            self.attachTableView.frame = frameT;
        }completion:^(BOOL finished) {
            [self resizeAddTextView:keyboardHeight];
        }];
    }
}


- (void)resizeAddTextView:(float)keyboardHeight {
    
    CGRect frame = self.detailTextView.frame;
    frame.size.height = self.view.frame.size.height - keyboardHeight - _toolBar.frame.size.height - frame.origin.y;
    
    NSLog(@"%lf", frame.size.height);
    
    [UIView animateWithDuration:0.01F animations:^{
        self.detailTextView.frame = frame;
    }completion:^(BOOL finished) {
        //
    }];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _arrAttach.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *imageCell   = @"identifierNoteCell";
    
    AttachFileTableViewCell *cell = (AttachFileTableViewCell*)[tableView dequeueReusableCellWithIdentifier:imageCell];
    if(cell == nil) {
        cell = [[AttachFileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:imageCell];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSString *fileName = [[[_arrAttach objectAtIndex:indexPath.row] m_file_name] lastPathComponent];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", _db.documentsDir, fileName];
    cell.lbfileName.text = fileName;
    
    NSData *imgData = [NSData dataWithContentsOfFile:filePath];
    UIImage *img = [[UIImage alloc] initWithData:imgData];
    cell.ivAttachFile.image = img;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    //오->왼으로 슬라이드시
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // 메모 삭제
        AttachDBMD *tempAttachDb = [_arrAttach objectAtIndex:indexPath.row];
        
        NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSError *error = nil;
        //for (NSString *file in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:folderPath error:&error]) {
        
        //}
        if(tempAttachDb.m_attachID){
//            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:tempAttachDb.m_file_name] error:&error];
//            [_db deleteAttach:tempAttachDb.m_attachID];
            [_tempArrAttach addObject:tempAttachDb];
        }else{
            NSString *fileName = [[tempAttachDb m_file_name] lastPathComponent];
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&error];
        }
        
        [_arrAttach removeObjectAtIndex:indexPath.row];
        [_attachTableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (IBAction)touchesPhotoBtn:(id)sender {
    
    _tempImage.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:_tempImage animated:YES completion:nil];
    
}

- (IBAction)touchesCameraBtn:(id)sender {
    
    _tempImage.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:_tempImage animated:YES completion:nil];
    
}

- (IBAction)touchesSaveBtn:(id)sender {
    
    NSString *contents = _detailTextView.text==nil?@"":_detailTextView.text;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    
    NoteDBMD *noteDBMD = [[NoteDBMD alloc] initWithNoteMD:_getNote.m_noteID m_folderID:_getNote.m_folderID m_contents:contents m_create_date:_getNote.m_create_date lat:_getNote.mLatitude lot:_getNote.mLongitude];
    [_db updateNoteId:noteDBMD];
    
    for(AttachDBMD *tempAttach in _arrAttach){
        NSInteger type = 0;
        
        NSString *fileName = [[tempAttach m_file_name] lastPathComponent];
        
        CFStringRef fileExtension = (CFStringRef) CFBridgingRetain(fileName.pathExtension);
        CFStringRef fileUTI =  UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, fileExtension, NULL);
        
        if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
            NSLog(@"It's an image");
            type = 0;
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeMovie)){
            NSLog(@"It's a movie");
            type = 1;
        }
        else if(UTTypeConformsTo(fileUTI, kUTTypeAudio)){
            NSLog(@"It's a audio");
            type = 2;
        }
        else if (UTTypeConformsTo(fileUTI, kUTTypeText)){
            NSLog(@"It's text");
            type = 3;
        }
        
        CFRelease(fileUTI);
        
        if(!tempAttach.m_attachID){ //새로 추가된 첨부파일만 저장.
            AttachDBMD *attachDBMD = [[AttachDBMD alloc] initWithAttachMD:-1 m_noteID:_getNo m_attach_type:type m_file_name:fileName];
            [_db insertAttach:attachDBMD];
        }
    }
    
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    
    for(AttachDBMD *temp in _tempArrAttach){
        [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:temp.m_file_name] error:&error];
        [_db deleteAttach:temp.m_attachID];
    }
    
    _detailTextView.text = @"";
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}



#pragma mark - uitableviewdelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    // 선택시 이동
    
    self->_sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PhotoListViewController *photoListViewController = [_sb instantiateViewControllerWithIdentifier:@"photolistviewcontroller"];
    
    photoListViewController.arrPhoto = _arrAttach;
    photoListViewController.getPhotoNo = indexPath.row;
    
    [photoListViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:photoListViewController animated:true completion:nil];
    
}



- (IBAction)touchesBtnShowTable:(id)sender {
    [_detailTextView resignFirstResponder];
}

- (IBAction)touchesCloseBtn:(id)sender {
    NSString *folderPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSError *error = nil;
    
    for(AttachDBMD *tempAttach in _arrAttach){
        if(!tempAttach.m_attachID){ //새로 추가된 첨부파일만 삭제
            NSString *fileName = [tempAttach.m_file_name lastPathComponent];
            [[NSFileManager defaultManager] removeItemAtPath:[folderPath stringByAppendingPathComponent:fileName] error:&error];
        }
    }
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark - UITextViewDelegate
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    return YES;
}

#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *strFindUniqueSavePath = [documentsPath objectAtIndex:0];
    strFindUniqueSavePath = [strFindUniqueSavePath stringByAppendingString:@"/"] ;
    
    NSString *tempSavePath = @"";
    
    UIGraphicsBeginImageContext(image.size);
    
    _strImageName = [self findUniqueSavePath:strFindUniqueSavePath];
    [_tempImageArray addObject:_strImageName];
    AttachDBMD *temp = [[AttachDBMD alloc] init];
    temp.m_file_name = _strImageName;
    [_arrAttach addObject:temp];
    NSLog(@"%@", _strImageName);
    
    NSArray *tempImageArr = [_strImageName componentsSeparatedByString:@"/"];
    _imagePath = [tempImageArr lastObject];
    
    tempSavePath = [strFindUniqueSavePath stringByAppendingString:_imagePath];
    
    [UIImageJPEGRepresentation(image, 1.0f) writeToFile:tempSavePath atomically:YES];
    UIGraphicsEndImageContext();
    
    
    // 지도용 이미지를 저장한다.
    UIImage *tempImage = [GenenalFun imageWithImage:image scaledResize:MAP_IMAGE_SIZE];
    UIGraphicsBeginImageContext(tempImage.size);
    NSArray *tempA = [_imagePath componentsSeparatedByString:@"."];
    
    tempSavePath = [strFindUniqueSavePath stringByAppendingString:[NSString stringWithFormat:@"%@_map.jpg", [tempA firstObject]]];
    [UIImageJPEGRepresentation(tempImage, 1.0f) writeToFile:tempSavePath atomically:YES];
    NSLog(@"%@", tempSavePath);
    UIGraphicsEndImageContext();
    // end of 지도용 이미지 저장
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)findUniqueSavePath:(NSString *)rootPath {
    int i = 0;
    NSString *path;
    
    do {
        path = [NSString stringWithFormat:@"%@/photo_%04d.jpg",rootPath, i++];
    } while ([[NSFileManager defaultManager] fileExistsAtPath:path]);
    
    return path;
}

// 취소할 경우
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
