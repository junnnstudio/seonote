//
//  SettingHiddenViewController.m
//  SeoNote
//
//  Created by 신서희 on 28/02/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "SettingHiddenViewController.h"
#import "SettingViewController.h"
#import "PasswordViewController.h"
#import "FolderMD.h"
#import "FolderDBMD.h"

#import "ConstantsDB.h"

@interface SettingHiddenViewController ()

@end

@implementation SettingHiddenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _arrFolder = [[NSMutableArray alloc] init];
    _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    _db = [[ConstantsDB alloc] initDatabase];
}

- (void)viewWillAppear:(BOOL)animated {
    _tempPw = [[NSUserDefaults standardUserDefaults] objectForKey:@"password"];
    
    _arrFolder = [[_db selectFolderAll] mutableCopy];
    [_settingHiddenTableView reloadData];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 45.0F;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0) {
        return 1;
    }else {
        return _arrFolder.count;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *hiddenUseCell = @"identifierIHiddenUseCell";
    static NSString *folderHiddenCell   = @"identifierFolderHiddenCell";
    
    if(indexPath.section == 0) {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:hiddenUseCell];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        _hiddenUseSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(cell.frame.size.width - 50, cell.frame.origin.y, 50.0f, 50.0f)];
        cell.accessoryView = _hiddenUseSwitch;
        [_hiddenUseSwitch addTarget:self action:@selector(updateSwitchAtIndexPath:) forControlEvents:UIControlEventTouchUpInside];

        if(_tempPw != nil){
            [_hiddenUseSwitch setOn:TRUE];
        }
        return cell;
        
    }else {
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:folderHiddenCell];
        if(cell == nil) {
            cell = [[UITableViewCell alloc] initWithFrame:CGRectZero];
            
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if(indexPath.row < _arrFolder.count){
            FolderDBMD *tempFolderDB = [_arrFolder objectAtIndex:indexPath.row];
            cell.textLabel.text = tempFolderDB.m_name;
            cell.textLabel.textColor = RGB(tempFolderDB.m_red, tempFolderDB.m_green, tempFolderDB.m_blue);
            if([tempFolderDB.m_hidden_yn  isEqual: @"Y"]){ //비밀번호 설정 되있는 폴더일때
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }else{
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section{
    NSString *tempTitle = @"";
    
    if(section == 0){
        tempTitle = @" Whether a password is used";
    }else{
        tempTitle = @" Notes Usage Status";
    }
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.text = tempTitle;
    header.textLabel.textColor = [UIColor lightGrayColor];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 40.0F;
}

#pragma mark - uitableviewdelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FolderDBMD *tempFolderDb = [_arrFolder objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if(cell.accessoryType == UITableViewCellAccessoryCheckmark){
        cell.accessoryType = UITableViewCellAccessoryNone;
        tempFolderDb.m_hidden_yn = @"N";
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        tempFolderDb.m_hidden_yn = @"Y";
    }
    
}

- (void)updateSwitchAtIndexPath:(NSIndexPath *)indexPath {
    //스위치 버튼 바뀔때마다 상태 업데이트.
    if([_hiddenUseSwitch isOn]){ //off -> on 될때 패스워드 설정화면으로 이동
        _sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        PasswordViewController *passwordViewController = [_sb instantiateViewControllerWithIdentifier:@"passwordviewcontroller"];
        
        [passwordViewController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:passwordViewController animated:true completion:nil];
    }else{ // on -> off 될때 설정된 패스워드 삭제
        _tempPw = nil;
        [[NSUserDefaults standardUserDefaults] setObject:_tempPw forKey:@"password"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)touchesCloseBtn:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)touchesSaveBtn:(id)sender {
    //스위치 버튼이 on일때만 선택한 폴더 암호화 설정 저장.
    if(_hiddenUseSwitch.isOn){
        for(FolderDBMD *tempFolder in _arrFolder){
            [_db updateFolderId:tempFolder];
        }
    }
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
