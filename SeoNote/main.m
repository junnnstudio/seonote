//
//  main.m
//  SeoNote
//
//  Created by hjunnn kim on 20/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
