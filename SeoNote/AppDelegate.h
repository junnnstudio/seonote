//
//  AppDelegate.h
//  SeoNote
//
//  Created by hjunnn kim on 20/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSDate *startDate;
@property (assign, nonatomic) NSInteger useSecond;


@end

