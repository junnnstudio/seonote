//
//  MapContentMD.h
//  SeoNote
//
//  Created by hjunnn kim on 02/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MapContentMD : NSObject

@property (assign, nonatomic) NSInteger mFolderID;
@property (assign, nonatomic) NSInteger mNoteID;
@property (strong, nonatomic) NSString *mCreateDate;
@property (assign, nonatomic) float mLatitude;
@property (assign, nonatomic) float mLongitude;
@property (strong, nonatomic) NSString *mHiddenYN;
@property (assign, nonatomic) NSInteger mRed;
@property (assign, nonatomic) NSInteger mGreen;
@property (assign, nonatomic) NSInteger mBlue;
@property (strong, nonatomic) NSString *mFileName;

- (id)init;
- (id)initWithMapContentMD:(NSInteger)folderID noteID:(NSInteger)noteID createDate:(NSString*)createDate lat:(float)latitude lot:(float)longitude hiddenYN:(NSString*)hiddenYN red:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue fileName:(NSString*)fileName;

@end

NS_ASSUME_NONNULL_END
