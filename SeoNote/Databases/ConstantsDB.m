//
//  ConstantsDB.m
//  SeoNote
//
//  Created by hjunnn kim on 28/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import "ConstantsDB.h"

#import "sqlite3.h"
#import "SQL.h"
#import "FolderDBMD.h"
#import "NoteDBMD.h"
#import "AttachDBMD.h"

#import "MapContentMD.h"

@implementation ConstantsDB

- (id)initDatabase {
    NSArray *documentsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _documentsDir = [documentsPath objectAtIndex:0];
    
    _dbName = DB_NAME;
    self.dbPath = [_documentsDir stringByAppendingPathComponent:self.dbName];
    
    return self;
}

- (void)checkAndCopyDB {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    if([fileManager fileExistsAtPath:_dbPath]){
        return;
    } else {
        NSString *databasePathFromApp = [[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:_dbName];
        [fileManager copyItemAtPath:databasePathFromApp toPath:self.dbPath error:&error];
    }
    
    if(error) {
        NSLog(@"DB Copy Error: %@", error);
    }
}

- (FolderDBMD*)selectFolderByfolderID:(NSInteger)idx {
    FolderDBMD *folderMD = [FolderDBMD new];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_FOLDER_BY_FOLDER_ID;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            sqlite3_bind_int(compiledStatment, 1, (int)idx);
            
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                NSInteger mID    = sqlite3_column_int(compiledStatment, 0);
                NSString *m_name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 1)];
                CGFloat m_red   = sqlite3_column_double(compiledStatment, 2);
                CGFloat m_green = sqlite3_column_double(compiledStatment, 3);
                CGFloat m_blue  = sqlite3_column_double(compiledStatment, 4);
                NSString *m_hidden_yn = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 5)];
                
                folderMD.idx = mID;
                folderMD.m_name = m_name;
                folderMD.m_red   = m_red;
                folderMD.m_green = m_green;
                folderMD.m_blue  = m_blue;
                folderMD.m_hidden_yn = m_hidden_yn;
            }
        }else {
            printf("ERROR SELECT_FOLDER_BY_FOLDER_ID ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return folderMD;
}

- (NSMutableArray<FolderDBMD*>*)selectFolderAll {
    NSMutableArray<FolderDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_FOLDER_ORDERBY_NAME_DESC;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                NSInteger mID   = sqlite3_column_int(compiledStatment, 0);
                NSString *m_name   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 1)];
                CGFloat m_red    = sqlite3_column_double(compiledStatment, 2);
                CGFloat m_green = sqlite3_column_double(compiledStatment, 3);
                CGFloat m_blue = sqlite3_column_double(compiledStatment, 4);
                NSString *m_hidden_yn = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 5)];
                
                FolderDBMD *folderDBMD = [[FolderDBMD alloc] initWithFolderMD:mID m_name:m_name m_red:m_red m_green:m_green m_blue:m_blue m_hidden_yn:m_hidden_yn];
                
                [tempArr addObject:folderDBMD];
            }
        }else {
            printf("ERROR SELECT_FOLDER_ORDERBY_NAME_DESC ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return tempArr;
}


-(void)insertFolder:(FolderDBMD *)folder{
    
    sqlite3 *db;
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char *sqlStatement = INSERT_FOLDER;
        sqlite3_stmt *compliedStatment;
        if(sqlite3_prepare_v2(db, sqlStatement,-1, &compliedStatment, NULL) == SQLITE_OK) {
            //            sqlite3_bind_int (compliedStatment, 1, [[arr objectAtIndex:0] intValue]);
            //            sqlite3_bind_int (compliedStatment, 2, [[arr objectAtIndex:1] intValue]);
            //            sqlite3_bind_int (compliedStatment, 3, [[arr objectAtIndex:2] intValue]);
            
            sqlite3_bind_text(compliedStatment, 1, [folder.m_name UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_double(compliedStatment, 2, folder.m_red);
            sqlite3_bind_double(compliedStatment, 3, folder.m_green);
            sqlite3_bind_double(compliedStatment, 4, folder.m_blue);
            sqlite3_bind_text(compliedStatment, 5, [folder.m_hidden_yn UTF8String], -1, SQLITE_TRANSIENT);
            if(SQLITE_DONE != sqlite3_step(compliedStatment)) {
                NSAssert1(0, @"Error while inserting into TB_FOLDER", sqlite3_errmsg(db));
            }
            sqlite3_reset(compliedStatment);
            sqlite3_close(db);
        }else { printf("Error INSERT INTO TB_FOLDER could not prepare statement: %s\n", sqlite3_errmsg(db)); }
    }else {
        sqlite3_close(db);
    }
}


-(void)insertNote:(NoteDBMD *)note{
    
    sqlite3 *db;
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char *sqlStatement = INSERT_NOTE;
        sqlite3_stmt *compliedStatment;
        if(sqlite3_prepare_v2(db, sqlStatement,-1, &compliedStatment, NULL) == SQLITE_OK) {
            //            sqlite3_bind_int (compliedStatment, 1, [[arr objectAtIndex:0] intValue]);
            sqlite3_bind_int (compliedStatment, 1, (int)note.m_folderID);
            
            sqlite3_bind_text(compliedStatment, 2, [note.m_contents UTF8String], -1, SQLITE_TRANSIENT);
            sqlite3_bind_text(compliedStatment, 3, [note.m_create_date UTF8String], -1, SQLITE_TRANSIENT);
            
            sqlite3_bind_double(compliedStatment, 4, (float)note.mLatitude);
            sqlite3_bind_double(compliedStatment, 5, (float)note.mLongitude);
            
            if(SQLITE_DONE != sqlite3_step(compliedStatment)) {
                NSAssert1(0, @"Error while inserting into TB_NOTE", sqlite3_errmsg(db));
            }
            sqlite3_reset(compliedStatment);
            sqlite3_close(db);
        }else { printf("Error INSERT INTO TB_NOTE could not prepare statement: %s\n", sqlite3_errmsg(db)); }
    }else {
        sqlite3_close(db);
    }
}


- (NSMutableArray<NoteDBMD*>*)selectNoteAll {
    NSMutableArray<NoteDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_NOTE_ORDERBY_CREATEDATE_DESC;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                
                NSInteger mID   = sqlite3_column_int(compiledStatment, 0);
                
                NSInteger m_folderID   = sqlite3_column_int(compiledStatment, 1);
                NSString *m_contents   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 2)];
                NSString *m_create_date   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 3)];
                float lat  = sqlite3_column_double(compiledStatment, 4);
                float lot  = sqlite3_column_double(compiledStatment, 5);
                
                NoteDBMD *noteDBMD = [[NoteDBMD alloc] initWithNoteMD:mID m_folderID:m_folderID m_contents:m_contents m_create_date:m_create_date lat:lat lot:lot];
                
                [tempArr addObject:noteDBMD];
            }
        }else {
            printf("ERROR SELECT_NOTE_ORDERBY_CREATEDATE_DESC ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return tempArr;
}

- (NSMutableArray<NoteDBMD*>*)selectNoteFolderId:(NSInteger)idx {
    NoteDBMD *noteDBMD = [[NoteDBMD alloc] init];
    NSMutableArray<NoteDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    sqlite3 *database;
    
    if(sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:SELECT_NOTE_FOLDER_ID, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                NSInteger mID   = sqlite3_column_int(compiledStatement, 0);
                
                NSInteger m_folderID   = sqlite3_column_int(compiledStatement, 1);
                NSString *m_contents   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *m_create_date   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                float mLat = sqlite3_column_double(compiledStatement, 4);
                float mLot = sqlite3_column_double(compiledStatement, 5);
                
                noteDBMD = [[NoteDBMD alloc] initWithNoteMD:mID m_folderID:m_folderID m_contents:m_contents m_create_date:m_create_date lat:mLat lot:mLot];
                [tempArr addObject:noteDBMD];
            }
            
            sqlite3_reset(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    return tempArr;
}

- (NSMutableArray<AttachDBMD*>*)selectAttachNoteId:(NSInteger)idx {
    AttachDBMD *attachDBMD = [[AttachDBMD alloc] init];
    NSMutableArray<AttachDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    //sqlite3 *db;
    
    sqlite3 *database;
    
    if(sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:SELECT_ATTACH_NOTE_ID, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                
                NSInteger m_attachID   = sqlite3_column_int(compiledStatement, 0);
                NSInteger m_noteID   = sqlite3_column_int(compiledStatement, 1);
                NSInteger m_attachType    = sqlite3_column_int(compiledStatement, 2);
                NSString *m_fileName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                
                attachDBMD = [[AttachDBMD alloc] initWithAttachMD:m_attachID m_noteID:m_noteID m_attach_type:m_attachType m_file_name:m_fileName];
                
                [tempArr addObject:attachDBMD];
            }
            
            sqlite3_reset(compiledStatement);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    
    
    
//    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
//        const char   *sqlStatement = SELECT_ATTACH_NOTE_ID;
//        sqlite3_stmt *compiledStatment = NULL;
//        
//        sqlite3_column_int(compiledStatment, (int)idx);
//        
//        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
//            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
//                
//                NSInteger m_attachID   = sqlite3_column_int(compiledStatment, 0);
//                NSInteger m_noteID   = sqlite3_column_int(compiledStatment, 1);
//                NSInteger m_attachType    = sqlite3_column_int(compiledStatment, 2);
//                NSString *m_fileName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 3)];
//                
//                attachDBMD = [[AttachDBMD alloc] initWithAttachMD:m_attachID m_noteID:m_noteID m_attach_type:m_attachType m_file_name:m_fileName];
//                
//                [tempArr addObject:attachDBMD];
//            }
//        }else {
//            printf("ERROR SELECT_ATTACH_NOTE_ID ALL prepare statement:  %s\n", sqlite3_errmsg(db));
//        }
//        sqlite3_finalize(compiledStatment);
//    }
//    sqlite3_close(db);
    
    return tempArr;
}


- (NSMutableArray<NoteDBMD*>*)selectSearchTextNote{
    NSMutableArray<NoteDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_SEARCH_TEXT_NOTE;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                
                NSInteger mID   = sqlite3_column_int(compiledStatment, 0);
                
                NSInteger m_folderID   = sqlite3_column_int(compiledStatment, 1);
                NSString *m_contents   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 2)];
                NSString *m_create_date   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 3)];
                float mLat = sqlite3_column_double(compiledStatment, 4);
                float mLot = sqlite3_column_double(compiledStatment, 5);
                
                NoteDBMD *noteDBMD = [[NoteDBMD alloc] initWithNoteMD:mID m_folderID:m_folderID m_contents:m_contents m_create_date:m_create_date lat:mLat lot:mLot];
                
                [tempArr addObject:noteDBMD];
            }
        }else {
            printf("ERROR SELECT_SEARCH_TEXT_NOTE ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return tempArr;
}

-(void)insertAttach:(AttachDBMD*)attach{
    
    sqlite3 *db;
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char *sqlStatement = INSERT_ATTACH;
        sqlite3_stmt *compliedStatment;
        if(sqlite3_prepare_v2(db, sqlStatement,-1, &compliedStatment, NULL) == SQLITE_OK) {
            //            sqlite3_bind_int (compliedStatment, 1, [[arr objectAtIndex:0] intValue]);
//            sqlite3_bind_int (compliedStatment, 1, attach.m_attachID);
            sqlite3_bind_int (compliedStatment, 1, (int)attach.m_noteID);
            sqlite3_bind_int (compliedStatment, 2, (int)attach.m_attach_type);
            sqlite3_bind_text(compliedStatment, 3, [attach.m_file_name UTF8String], -1, SQLITE_TRANSIENT);

            
            if(SQLITE_DONE != sqlite3_step(compliedStatment)) {
                NSAssert1(0, @"Error while inserting into TB_ATTACH", sqlite3_errmsg(db));
            }
            sqlite3_reset(compliedStatment);
            sqlite3_close(db);
        }else { printf("Error INSERT INTO INSERT_ATTACH could not prepare statement: %s\n", sqlite3_errmsg(db)); }
    }else {
        sqlite3_close(db);
    }
}


-(void)deleteNote:(NSInteger)idx{
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:DELETE_NOTE, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            if(SQLITE_DONE != sqlite3_step(compiledStatment))
                NSAssert1(0, @"Error while delete. '%s'", sqlite3_errmsg(db));
            
            sqlite3_reset(compiledStatment);
        }else {
            printf("ERROR DELETE_NOTE ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
}

-(void)deleteNoteByFolderID:(NSInteger)fid {
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:DELETE_NOTE_BY_FID, (long)fid]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            if(SQLITE_DONE != sqlite3_step(compiledStatment))
                NSAssert1(0, @"Error while delete. '%s'", sqlite3_errmsg(db));
            
            sqlite3_reset(compiledStatment);
        }else {
            printf("ERROR DELETE_NOTE_BY_FID ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
}

-(void)deleteFolder:(NSInteger)idx{
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:DELETE_FOLDER, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            if(SQLITE_DONE != sqlite3_step(compiledStatment))
                NSAssert1(0, @"Error while delete. '%s'", sqlite3_errmsg(db));
            
            sqlite3_reset(compiledStatment);
        }else {
            printf("ERROR DELETE_FOLDER ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
}


-(void)deleteAttach:(NSInteger)idx{
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:DELETE_ATTACH, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            if(SQLITE_DONE != sqlite3_step(compiledStatment))
                NSAssert1(0, @"Error while delete. '%s'", sqlite3_errmsg(db));
            
            sqlite3_reset(compiledStatment);
        }else {
            printf("ERROR DELETE_ATTACH ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
}

-(void)deleteAttachByNoteID:(NSInteger)nid {
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:DELETE_ATTACH_BY_NID, (long)nid]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            if(SQLITE_DONE != sqlite3_step(compiledStatment))
                NSAssert1(0, @"Error while delete. '%s'", sqlite3_errmsg(db));
            
            sqlite3_reset(compiledStatment);
        }else {
            printf("ERROR DELETE_ATTACH_BY_NID ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
}



- (NSMutableArray<AttachDBMD*>*)selectAttachAll {
    AttachDBMD *attachDBMD = [[AttachDBMD alloc] init];
    NSMutableArray<AttachDBMD*> *tempArr = [[NSMutableArray alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_ATTACH_ORDERBY_ID_DESC;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                
                NSInteger m_attachID   = sqlite3_column_int(compiledStatment, 0);
                NSInteger m_noteID   = sqlite3_column_int(compiledStatment, 1);
                NSInteger m_attachType    = sqlite3_column_int(compiledStatment, 2);
                NSString *m_fileName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 3)];
                
                attachDBMD = [[AttachDBMD alloc] initWithAttachMD:m_attachID m_noteID:m_noteID m_attach_type:m_attachType m_file_name:m_fileName];
                
                [tempArr addObject:attachDBMD];
            }
        }else {
            printf("ERROR SELECT_ATTACH_ORDERBY_ID_DESC ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return tempArr;
}




- (NoteDBMD*)selectNoteLimitOne {
    NoteDBMD *noteDBMD = [[NoteDBMD alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_NOTE_ORDERBY_ID_DESC_LIMIT_1;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                NSInteger mID   = sqlite3_column_int(compiledStatment, 0);
                
                NSInteger m_folderID   = sqlite3_column_int(compiledStatment, 1);
                NSString *m_contents   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 2)];
                NSString *m_create_date   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 3)];
                float mLat = sqlite3_column_double(compiledStatment, 4);
                float mLot = sqlite3_column_double(compiledStatment, 5);
                
                noteDBMD = [[NoteDBMD alloc] initWithNoteMD:mID m_folderID:m_folderID m_contents:m_contents m_create_date:m_create_date lat:mLat lot:mLot];
            }
        }else {
            printf("ERROR SELECT_ATTACH_NOTE_ID ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return noteDBMD;
}



- (NoteDBMD*)selectNoteId:(NSInteger)idx {
    NoteDBMD *noteDBMD = [[NoteDBMD alloc] init];
    
    sqlite3 *database;
    
    if(sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:SELECT_NOTE_ID, (long)idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                NSInteger mID   = sqlite3_column_int(compiledStatement, 0);
                
                NSInteger m_folderID   = sqlite3_column_int(compiledStatement, 1);
                NSString *m_contents   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 2)];
                NSString *m_create_date   = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatement, 3)];
                float mLat = sqlite3_column_double(compiledStatement, 4);
                float mLot = sqlite3_column_double(compiledStatement, 5);
                
                noteDBMD = [[NoteDBMD alloc] initWithNoteMD:mID m_folderID:m_folderID m_contents:m_contents m_create_date:m_create_date lat:mLat lot:mLot];
            }
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
    return noteDBMD;
}



- (void)updateNoteId:(NoteDBMD *)note{
    sqlite3 *database;
        
    if(sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:UPDATE_NOTE,note.m_folderID, note.m_contents, (long)note.m_noteID]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
//            sqlite3_bind_int (compiledStatement, 1, note.m_folderID);
//
//            sqlite3_bind_text(compiledStatement, 2, [note.m_contents UTF8String], -1, SQLITE_TRANSIENT);
            if(SQLITE_DONE != sqlite3_step(compiledStatement)) {
                NSAssert1(0, @"Error while update into TB_NOTE", sqlite3_errmsg(database));
            }
            sqlite3_reset(compiledStatement);
            sqlite3_close(database);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
}


- (void)updateFolderId:(FolderDBMD *)folder{
    sqlite3 *database;
    
    if(sqlite3_open([self.dbPath UTF8String], &database) == SQLITE_OK) {
        NSString *sqlStatementNS = [[NSString alloc] initWithString:[NSString stringWithFormat:UPDATE_FOLDER,folder.m_hidden_yn, (long)folder.idx]];
        const char *sqlStatement = [sqlStatementNS UTF8String];
        sqlite3_stmt *compiledStatement;
        
        if(sqlite3_prepare_v2(database, sqlStatement, -1, &compiledStatement, NULL) == SQLITE_OK) {
            //            sqlite3_bind_int (compiledStatement, 1, note.m_folderID);
            //
            //            sqlite3_bind_text(compiledStatement, 2, [note.m_contents UTF8String], -1, SQLITE_TRANSIENT);
            if(SQLITE_DONE != sqlite3_step(compiledStatement)) {
                NSAssert1(0, @"Error while update into TB_NOTE", sqlite3_errmsg(database));
            }
            sqlite3_reset(compiledStatement);
            sqlite3_close(database);
        }
        sqlite3_finalize(compiledStatement);
    }
    sqlite3_close(database);
}



- (NSMutableArray<MapContentMD*>*)selectMapContent {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    
    sqlite3 *db;
    
    if(sqlite3_open([self.dbPath UTF8String], &db) == SQLITE_OK) {
        const char   *sqlStatement = SELECT_MAP_DATA;
        sqlite3_stmt *compiledStatment;
        
        if(sqlite3_prepare_v2(db, sqlStatement, -1, &compiledStatment, NULL) == SQLITE_OK) {
            while(sqlite3_step(compiledStatment) == SQLITE_ROW) {
                NSInteger mFolderID = sqlite3_column_int(compiledStatment, 0);
                NSInteger mNoteID   = sqlite3_column_int(compiledStatment, 1);
                NSString *mCreateDate = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 2)];
                float mLatitude  = sqlite3_column_double(compiledStatment, 3);
                float mLongitude = sqlite3_column_double(compiledStatment, 4);
                NSString *mHiddenYN = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 5)];
                NSInteger mRed   = sqlite3_column_int(compiledStatment, 6);
                NSInteger mGreen = sqlite3_column_int(compiledStatment, 7);
                NSInteger mBlue  = sqlite3_column_int(compiledStatment, 8);
                NSString *mFileName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(compiledStatment, 9)];
                
                MapContentMD *md = [[MapContentMD alloc] initWithMapContentMD:mFolderID noteID:mNoteID createDate:mCreateDate lat:mLatitude lot:mLongitude hiddenYN:mHiddenYN red:mRed green:mGreen blue:mBlue fileName:mFileName];
                
                [arr addObject:md];
            }
        }else {
            printf("ERROR SELECT_MAP_DATA ALL prepare statement:  %s\n", sqlite3_errmsg(db));
        }
        sqlite3_finalize(compiledStatment);
    }
    sqlite3_close(db);
    
    return arr;
}


@end
