//
//  AttachDBMD.h
//  SeoNote
//
//  Created by 신서희 on 21/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttachDBMD : NSObject

@property (nonatomic) NSInteger m_attachID;
@property (nonatomic) NSInteger m_noteID;
@property (nonatomic) NSInteger m_attach_type; //0:사진 1:동영상 2:음성
@property (nonatomic) NSString *m_file_name;


- (id)init;
- (id)initWithAttachMD:(NSInteger)m_attachID m_noteID:(NSInteger)m_noteID m_attach_type:(NSInteger)m_attach_type m_file_name:(NSString*)m_file_name;

@end

NS_ASSUME_NONNULL_END
