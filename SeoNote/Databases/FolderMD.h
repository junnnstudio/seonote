//
//  FolderMD.h
//  SeoNote
//
//  Created by 신서희 on 2019. 1. 5..
//  Copyright © 2019년 hjunnn kim. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FolderMD : JSONModel

@property (nonatomic) NSString<Optional>* m_name;
@property (nonatomic) CGFloat m_red;
@property (nonatomic) CGFloat m_green;
@property (nonatomic) CGFloat m_blue;
@property (nonatomic) NSInteger idx;
@property (nonatomic) NSString<Optional>* m_hidden_yn;

- (instancetype)initWithJSONString:(NSString *)JSONString;

@end

NS_ASSUME_NONNULL_END
