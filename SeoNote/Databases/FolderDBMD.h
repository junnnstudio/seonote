//
//  FolderDBMD.h
//  SeoNote
//
//  Created by 신서희 on 2019. 1. 5..
//  Copyright © 2019년 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FolderDBMD : NSObject


@property(assign, nonatomic) NSInteger idx;
@property(strong, nonatomic) NSString *m_name;
@property(assign, nonatomic) CGFloat m_red;
@property(assign, nonatomic) CGFloat m_green;
@property(assign, nonatomic) CGFloat m_blue;
@property(strong, nonatomic) NSString *m_hidden_yn;


- (id)init;
- (id)initWithFolderMD:(NSInteger)idx m_name:(NSString*)m_name m_red:(CGFloat)m_red m_green:(CGFloat)m_green m_blue:(CGFloat)m_blue m_hidden_yn:(NSString*)m_hidden_yn;

@end

NS_ASSUME_NONNULL_END
