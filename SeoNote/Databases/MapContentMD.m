//
//  MapContentMD.m
//  SeoNote
//
//  Created by hjunnn kim on 02/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "MapContentMD.h"

@implementation MapContentMD

- (id)init {
    self = [super init];
    if(self) {
        self.mFolderID = 0;
        self.mNoteID   = 0;
        self.mCreateDate = @"";
        self.mLatitude   = 0;
        self.mLongitude  = 0;
        self.mHiddenYN   = @"N";
        self.mRed   = 0;
        self.mGreen = 0;
        self.mBlue  = 0;
        self.mFileName  = @"";
    }
    
    return self;
}

- (id)initWithMapContentMD:(NSInteger)folderID noteID:(NSInteger)noteID createDate:(NSString*)createDate lat:(float)latitude lot:(float)longitude hiddenYN:(NSString*)hiddenYN red:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue fileName:(NSString*)fileName {
    
    self.mFolderID = folderID;
    self.mNoteID   = noteID;
    self.mCreateDate = createDate;
    self.mLatitude   = latitude;
    self.mLongitude  = longitude;
    self.mHiddenYN   = hiddenYN;
    self.mRed   = red;
    self.mGreen = green;
    self.mBlue  = blue;
    self.mFileName  = fileName;
    
    return self;
}

@end
