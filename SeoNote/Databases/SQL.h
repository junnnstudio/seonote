//
//  SQL.h
//  SeoNote
//
//  Created by hjunnn kim on 28/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#ifndef SQL_h
#define SQL_h


#define DB_NAME   @"seonote.db"

#define SELECT_FOLDER_BY_FOLDER_ID   "SELECT m_folderID, m_name, m_red, m_green, m_blue, m_hidden_yn from TB_FOLDER WHERE m_folderID = ?"

#define SELECT_FOLDER_ORDERBY_NAME_DESC   "SELECT m_folderID, m_name, m_red, m_green, m_blue, m_hidden_yn from TB_FOLDER ORDER BY m_name desc"

#define SELECT_NOTE_ORDERBY_CREATEDATE_DESC   "SELECT m_noteID, m_folderID, m_contents, m_create_date, m_latitude, m_longitude from TB_NOTE ORDER BY m_noteID desc"

#define SELECT_NOTE_ID   @"SELECT m_noteID, m_folderID, m_contents, m_create_date, m_latitude, m_longitude from TB_NOTE WHERE m_noteID = %ld"

#define SELECT_NOTE_ORDERBY_ID_DESC_LIMIT_1   "SELECT m_noteID, m_folderID, m_contents, m_create_date, m_latitude, m_longitude from TB_NOTE ORDER BY m_noteID desc limit 1"

#define SELECT_ATTACH_NOTE_ID   @"SELECT m_attachID, m_noteID, m_attach_type, m_file_name from TB_ATTACH WHERE m_noteID = %ld"

#define SELECT_ATTACH_ORDERBY_ID_DESC  "SELECT m_attachID, m_noteID, m_attach_type, m_file_name from TB_ATTACH ORDER BY m_attachID"

#define SELECT_NOTE_FOLDER_ID   @"SELECT m_noteID, m_folderID, m_contents, m_create_date, m_latitude, m_longitude from TB_NOTE WHERE m_folderID = %ld"

#define SELECT_SEARCH_TEXT_NOTE   " SELECT m_noteID, m_folderID, m_contents, m_create_date, m_latitude, m_longitude from TB_NOTE WHERE m_folderID = 0 UNION ALL SELECT TB_NOTE.m_noteID, TB_NOTE.m_folderID, TB_NOTE.m_contents, TB_NOTE.m_create_date, TB_NOTE.m_latitude, TB_NOTE.m_longitude  from TB_NOTE JOIN TB_FOLDER ON TB_NOTE.m_folderID = TB_FOLDER.m_folderID WHERE TB_FOLDER.m_hidden_yn = 'N' "

#define INSERT_NOTE     "INSERT INTO TB_NOTE(m_folderID, m_contents, m_create_date, m_latitude, m_longitude) Values(?, ?, ?, ?, ?) "
#define INSERT_FOLDER   "INSERT INTO TB_FOLDER(m_name, m_red, m_green, m_blue, m_hidden_yn) Values(?, ?, ?, ?, ?) "
#define INSERT_ATTACH   "INSERT INTO TB_ATTACH(m_noteID, m_attach_type, m_file_name) Values(?, ?, ?) "

#define DELETE_NOTE             @"DELETE FROM TB_NOTE WHERE m_noteID = %ld"
#define DELETE_NOTE_BY_FID      @"DELETE FROM TB_NOTE WHERE m_folderID = %ld"
#define DELETE_FOLDER           @"DELETE FROM TB_FOLDER WHERE m_folderID = %ld"
#define DELETE_ATTACH           @"DELETE FROM TB_ATTACH WHERE m_attachID = %ld"
#define DELETE_ATTACH_BY_NID    @"DELETE FROM TB_ATTACH WHERE m_noteID = %ld"

#define UPDATE_NOTE @"UPDATE TB_NOTE SET m_folderID = %ld, m_contents = '%@' WHERE m_noteID = %ld"
#define UPDATE_FOLDER @"UPDATE TB_FOLDER SET m_hidden_yn = '%@' WHERE m_folderID = %ld"


#define SELECT_MAP_DATA "SELECT n.m_folderID, n.m_noteID, n.m_create_date, n.m_latitude, n.m_longitude, IFNULL(f.m_hidden_yn, 'N'), IFNULL(f.m_red, 0), IFNULL(f.m_green, 0), IFNULL(f.m_blue, 0), a.m_file_name FROM TB_ATTACH AS a LEFT JOIN TB_NOTE AS n ON a.m_noteID = n.m_noteID LEFT JOIN TB_FOLDER AS f ON n.m_folderID = f.m_folderID order By n.m_create_date desc, n.m_noteID desc, a.m_file_name asc"

#endif /* SQL_h */
