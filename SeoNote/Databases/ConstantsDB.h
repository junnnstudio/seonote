//
//  ConstantsDB.h
//  SeoNote
//
//  Created by hjunnn kim on 28/12/2018.
//  Copyright © 2018 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FolderDBMD;
@class NoteDBMD;
@class AttachDBMD, MapContentMD;

NS_ASSUME_NONNULL_BEGIN

@interface ConstantsDB : NSObject

@property (strong, nonatomic) NSString *dbName;
@property (strong, nonatomic) NSString *dbPath;
@property (strong, nonatomic) NSString *documentsDir;

- (id)initDatabase;
- (void)checkAndCopyDB;

- (FolderDBMD*)selectFolderByfolderID:(NSInteger)idx;

- (NSMutableArray<FolderDBMD*>*)selectFolderAll;
- (NSMutableArray<NoteDBMD*>*)selectNoteAll;
- (NSMutableArray<NoteDBMD*>*)selectNoteFolderId:(NSInteger)idx;
- (NSMutableArray<AttachDBMD*>*)selectAttachNoteId:(NSInteger)idx;
- (NoteDBMD*)selectNoteId:(NSInteger)idx;
- (NSMutableArray<AttachDBMD*>*)selectAttachAll;
- (NoteDBMD*)selectNoteLimitOne;
- (NSMutableArray<NoteDBMD*>*)selectSearchTextNote;
- (void) insertFolder:(FolderDBMD*)folder;
- (void) insertNote:(NoteDBMD*)note;
- (void) insertAttach:(AttachDBMD*)attach;

- (void)deleteNote:(NSInteger)idx;
- (void)deleteNoteByFolderID:(NSInteger)fid;
- (void)deleteFolder:(NSInteger)idx;
- (void)deleteAttach:(NSInteger)idx;
- (void)deleteAttachByNoteID:(NSInteger)nid;

- (void)updateNoteId:(NoteDBMD *)note;
- (void)updateFolderId:(FolderDBMD *)folder;



- (NSMutableArray<MapContentMD*>*)selectMapContent;

@end

NS_ASSUME_NONNULL_END
