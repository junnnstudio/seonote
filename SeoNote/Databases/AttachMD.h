//
//  AttachMD.h
//  SeoNote
//
//  Created by 신서희 on 21/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AttachMD : JSONModel

@property (nonatomic) NSInteger m_attachID;
@property (nonatomic) NSInteger m_noteID;
@property (nonatomic) NSInteger m_attach_type; //0:사진 1:동영상 2:음성
@property (nonatomic) NSString *m_file_name;

- (instancetype)initWithJSONString:(NSString *)JSONString;


@end

NS_ASSUME_NONNULL_END
