//
//  FolderDBMD.m
//  SeoNote
//
//  Created by 신서희 on 2019. 1. 5..
//  Copyright © 2019년 hjunnn kim. All rights reserved.
//

#import "FolderDBMD.h"

@implementation FolderDBMD

- (id)init {
    self = [super init];
    if(self) {
        self.idx = 1;
        self.m_name = @"";
        self.m_red = 0.0f;
        self.m_green = 0.0f;
        self.m_blue = 0.0f;
        self.m_hidden_yn = @"";
    }
    return self;
}

- (id)initWithFolderMD:(NSInteger)idx m_name:(NSString*)m_name m_red:(CGFloat)m_red m_green:(CGFloat)m_green m_blue:(CGFloat)m_blue m_hidden_yn:(NSString*)m_hidden_yn{
    self.idx = idx;
    self.m_name = m_name;
    self.m_red = m_red;
    self.m_green = m_green;
    self.m_blue = m_blue;
    self.m_hidden_yn = m_hidden_yn;
    
    return self;
}

@end
