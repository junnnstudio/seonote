//
//  MemoDBMD.h
//  HiddenMemo
//
//  Created by hjunnn kim on 04/12/2018.
//  Copyright © 2018 ssh. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoteDBMD : NSObject

@property (nonatomic) NSInteger m_noteID;
@property (nonatomic) NSInteger m_folderID;
@property (nonatomic) NSString *m_contents;
@property (nonatomic) NSString *m_create_date;
@property (assign, nonatomic) float mLatitude;
@property (assign, nonatomic) float mLongitude;

- (id)init;
- (id)initWithNoteMD:(NSInteger)m_noteID m_folderID:(NSInteger)m_folderID m_contents:(NSString*)m_contents m_create_date:(NSString*)m_create_date lat:(float)latitude lot:(float)longitude;

@end

NS_ASSUME_NONNULL_END
