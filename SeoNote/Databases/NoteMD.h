//
//  NoteMD.h
//  SeoNote
//
//  Created by 신서희 on 08/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoteMD : JSONModel

@property (nonatomic) NSInteger m_noteID;
@property (nonatomic) NSInteger m_folderID;
@property (nonatomic) NSString *m_contents;
@property (nonatomic) NSString *m_create_date;

- (instancetype)initWithJSONString:(NSString *)JSONString;

@end

NS_ASSUME_NONNULL_END
