//
//  AttachDBMD.m
//  SeoNote
//
//  Created by 신서희 on 21/01/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "AttachDBMD.h"

@implementation AttachDBMD

- (id)init {
    self = [super init];
    if(self) {
        self.m_attachID = 0;
        self.m_noteID = 0;
        self.m_attach_type = 0;
        self.m_file_name = @"";
    }
    return self;
}

- (id)initWithAttachMD:(NSInteger)m_attachID m_noteID:(NSInteger)m_noteID m_attach_type:(NSInteger)m_attach_type m_file_name:(NSString*)m_file_name {
    self.m_attachID = m_attachID;
    self.m_noteID = m_noteID;
    self.m_attach_type = m_attach_type;
    self.m_file_name = m_file_name;
    
    return self;
}

@end
