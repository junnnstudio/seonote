//
//  MemoDBMD.m
//  HiddenMemo
//
//  Created by hjunnn kim on 04/12/2018.
//  Copyright © 2018 ssh. All rights reserved.
//

#import "NoteDBMD.h"

@implementation NoteDBMD

- (id)init {
    self = [super init];
    if(self) {
        self.m_noteID = 0;
        self.m_folderID = 0;
        self.m_contents = @"";
        self.m_create_date = @"1999-01-01 00:00:00";
        
        self.mLatitude  = 0;
        self.mLongitude = 0;
    }
    return self;
}

- (id)initWithNoteMD:(NSInteger)m_noteID m_folderID:(NSInteger)m_folderID m_contents:(NSString*)m_contents m_create_date:(NSString*)m_create_date lat:(float)latitude lot:(float)longitude {
    self.m_noteID = m_noteID;
    self.m_folderID = m_folderID;
    self.m_contents = m_contents;
    self.m_create_date = m_create_date;
    
    self.mLatitude  = latitude;
    self.mLongitude = longitude;
    
    return self;
}


@end
