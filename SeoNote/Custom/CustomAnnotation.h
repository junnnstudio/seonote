//
//  CustomAnnotation.h
//  SeoNote
//
//  Created by hjunnn kim on 02/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>

@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface CustomAnnotation : NSObject<MKAnnotation>

@property (copy, nonatomic) NSString * title;
@property (copy, nonatomic) NSString * name;
@property (copy, nonatomic) NSString * subtitle;
@property (assign, nonatomic) CLLocationCoordinate2D coordinate;

@property (strong, nonatomic) NSString *imgName;
@property (assign, nonatomic) NSInteger noteID; 

@end

NS_ASSUME_NONNULL_END
