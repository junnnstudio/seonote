//
//  GenenalFun.h
//  SeoNote
//
//  Created by hjunnn kim on 03/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GenenalFun : NSObject

+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledResize:(float)size;

@end

NS_ASSUME_NONNULL_END
