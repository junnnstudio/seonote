//
//  GenenalFun.m
//  SeoNote
//
//  Created by hjunnn kim on 03/04/2019.
//  Copyright © 2019 hjunnn kim. All rights reserved.
//

#import "GenenalFun.h"

@implementation GenenalFun

+ (UIImage*)imageWithImage:(UIImage*)sourceImage scaledResize:(float)size {
    BOOL isWidthLong = NO;
    
    if (sourceImage.size.width > sourceImage.size.height) {
        if(sourceImage.size.width <= size)
            return sourceImage;   // 가로가 큰데 1080보다 작거나 같다면 사이즈 조절할 필요가없음.
        isWidthLong = YES;
    }else {
        if(sourceImage.size.height <= size)
            return sourceImage;   // 세로가 큰데 1080보다 작거나 같다면 사이즈 조절 X
        isWidthLong = NO;
    }
    
    if(isWidthLong) {   //가로가 크다면
        float oldWidth = sourceImage.size.width;
        float scaleFactor = size / oldWidth;
        
        float newHeight = sourceImage.size.height * scaleFactor;
        float newWidth = oldWidth * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }else {   //세로가 크다면
        float oldHeight = sourceImage.size.height;
        float scaleFactor = size / oldHeight;
        
        float newWidth = sourceImage.size.width * scaleFactor;
        float newHeight = oldHeight * scaleFactor;
        
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
        [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return newImage;
    }
    
    return sourceImage;
}

@end
